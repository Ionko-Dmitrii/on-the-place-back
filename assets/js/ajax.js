// Get Cookie Method
const getCookie = function (name) {
    var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined // s if r == 2 else b тернарный оператор
};

// Скрипт для отправки ссылки для восстановаления пароля
$("#get_link_for_new_password").on('submit', function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var $this = $(this);

    $.ajax({
        url: url,
        method: 'POST',
        data: $this.serialize(),

        success: function (data) {
            alert("Ссылка отправлена на ваш емайл")
        },
        error: function (error) {
            if (error.responseJSON) {
                alert(error.responseJSON.message)
            } else {
                alert('Что то пошло не так!')
            }
        }
    })
})

// Преобразовывает дату рождения в нужный формат
$('.form-box-birthday__grid .form-birthday').on('change keyup', function () {
    var date = $('.form-box-birthday__grid .form-box-birthday__item:eq(0) .form-birthday').val() + '.'
            + $('.form-box-birthday__grid .form-box-birthday__item:eq(1) .form-birthday').val() + '.'
            + $('.form-box-birthday__grid .form-box-birthday__item:eq(2) .form-birthday').val()
    $("input[name='birthday']").val(date)
})

// Преобразовывает любимые виды спорта в нужный формат
$("select[name='favorite_sport_select']").on("change", function () {
    var selectVal = $(this).val()
    $("input[name='favorite_sport']").val(selectVal)
})

// Скрипт для регистрации
$("#registration").on('submit', function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var $this = $(this);
    $this.find('.form-error-msg').removeClass('error').text("")
    $this.find('button').attr('disabled', true)

    $.ajax({
        url: url,
        type: 'post',
        data: $this.serialize(),

        success: function (data) {
            $this.find('button').removeAttr('disabled')
            document.location.replace(window.location.origin + data.url)
        },
        error: function (errors) {
            $this.find('button').removeAttr('disabled')
            const errorObj = {}
            if (errors.responseJSON) {
                errors.responseJSON.message.forEach(function (item) {
                    errorObj[item[0]] = item[1]
                    if ($this.find(`.form-field[name=${item[0]}]`).closest('.form-box').children('.form-error-msg').hasClass('error')) {
                        $this.find(`.form-field[name=${item[0]}]`).closest('.form-box').children('.form-error-msg')
                                .removeClass('error').text("")
                    } else {
                        $this.find(`.form-field[name=${item[0]}]`).closest('.form-box').children('.form-error-msg')
                                .addClass('error').text(item[1])
                    }
                })
                if (errorObj.__all__) {
                    $(".form-field[name='password']").closest(".form-box").children('.form-error-msg').addClass('error').text(errorObj.__all__[0])
                }
            }
        }
    })
})

$('.product-detail').on('change', '.filter-input-checkbox', function () {
    var $this = $(this);
    added_checked_for_size($this);
})

function added_checked_for_size($this) {
    var nameInput = $this.attr("name");
    if ($this.hasClass("active")) {
        $(`input[name='${nameInput}']`).removeClass("active")
        $(`input[name='${nameInput}']`).prop('checked', false);
    } else {
        $(`input[name='${nameInput}']`).addClass("active")
        $(`input[name='${nameInput}']`).prop('checked', true);
    }
}

//Скрипт для фильтрации товаров
var timeoutRequest = null;
var filterDataDict = {};
var url = $('.filter-form-wrapper').attr('action');
var csrfToken = $(".filter-form-wrapper input[name='csrfmiddlewaretoken']").val();
var blockForPushContent = $('.product-list .preloader');

$('.filter-form-wrapper .filter-checkbox-but').on("change keyup", function () {
    var categoryName = $(this).closest('.filter-item').data('name');
    var itemId = $(this).val().trim();
    var nameInput = $(this).attr("name");
    var $this = $(this);
    added_checked_for_size($this)

    if (categoryName in filterDataDict) {
        var filterArr = filterDataDict[categoryName]
        var indexItem = filterArr.indexOf(itemId)
        if ($(this).attr('type') === 'checkbox') {
            if (indexItem !== -1) {
                filterArr.splice(indexItem, 1)
                if (filterArr.length === 0) {
                    delete filterDataDict[categoryName]
                }
            } else {
                filterArr.push(itemId)
            }
        } else {
            if (filterArr[0] === "") {
                delete filterDataDict[categoryName]
            } else {
                filterArr.length = 0;
                filterArr.push(itemId)
            }
        }

    } else {
        filterDataDict[categoryName] = [itemId]
    }

    if (timeoutRequest) clearTimeout(timeoutRequest);
    timeoutRequest = setTimeout(function () {
        $('.product-list .preloader').css({
            'opacity': '1',
            'visibility': 'visible'
        })

        $.ajax({
            url: url,
            type: 'post',
            data: {
                categoryId: JSON.stringify(filterDataDict),
                csrfmiddlewaretoken: csrfToken,
            },

            success: function (data) {
                blockForPushContent.css({
                    'opacity': '0',
                    'visibility': 'hidden'
                })
                blockForPushContent.nextAll().remove()
                blockForPushContent.after(data.products_html)
                if ($('.generator-carousel').length > 0) {
                    $('.generator-carousel').children().remove();
                    $('.generator-carousel').append(data.products_html);
                    $('.product-info__render .aside-config').replaceWith(data.products_html_info);
                    getSlickSlider();
                    addedActiveClass();
                    calculate_sum();
                    my_range()
                }
            },

            error: function (errors) {
                $('.product-list .preloader').css({
                    'opacity': '0',
                    'visibility': 'hidden'
                })
                console.log('error')
            }
        })
    }, 1000)

})


//Скрипт для пагинации новостей
// next page click
var pagination = $(".pagination");
var csrfTokenPagination = $(".pagination input[name='csrfmiddlewaretoken']").val();
var paginationUrl = pagination.attr('action');
var profileNews = $('.profile-news');
var modalPageError = $('.modal-error');

profileNews.on('click', '.pagination-arrow', function (e) {
    e.preventDefault();
    let pageNumber = $(this).data('page');
    newsPaginationAjax(paginationUrl, csrfTokenPagination, pageNumber)
});

profileNews.on('submit', pagination, function (e) {
    e.preventDefault();
    let pageNumber = $('.pagination-input').val();
    newsPaginationAjax(paginationUrl, csrfTokenPagination, pageNumber);
});

function newsPaginationAjax(paginationUrl, csrfTokenPagination, pageNumber) {
    $.ajax({
        url: paginationUrl,
        method: 'POST',
        data: {
            csrfmiddlewaretoken: csrfTokenPagination,
            page_number: pageNumber
        },
        dataType: 'json',

        success: function (data) {
            updateContext(data);
            updateUrlParams(pageNumber);
        },
        error: function (error) {

            if (error.responseJSON) {
                modalPageError.css('display', 'flex');
                console.log(error.responseJSON.message)
            } else {
                modalPageError.css('display', 'flex')
            }
        }
    })
}

$('.modal-btn').on('click', function (e) {
    e.preventDefault();
    modalPageError.css('display', 'none');
});

var updateContext = function (context) {
    $('.pagination').remove();
    $('.profile-news-grid').remove();
    profileNews.append(context.profile_news);
    profileNews.append(context.profile_news_pagination);
};

var updateUrlParams = function (value) {
    let url = new URL(window.location.href);
    url.searchParams.set('page', value);
    window.history.replaceState(null, '', url)

};

// Скрипт для добавления товара в избранное
$(document).on('click', '.add_to_favorite', function () {
    var productId = $(this).data('id');
    var url = $(this).data('url');
    var thisItem = $(this);
    thisItem.css('pointer-events', 'none');

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            product_id: String(productId),
        },

        success: function (data) {
            thisItem.css('pointer-events', 'auto');
            thisItem.addClass('active')
            if (data.added_or_remove === 1) {
                thisItem.addClass('active');
            } else {
                thisItem.removeClass('active');
            }
        },

        error: function (errors) {
            thisItem.css('pointer-events', 'auto');
            alert("Что то пошло не так!")
        }
    })
});

// Скрипт для оформления заказа на конструктор форм
$("#constructor-form").on("submit", function (e) {
    e.preventDefault();
    var $this = $(this);
    var url = $this.attr('action');
    var DictOrderInfo = {};
    var activeProducts = $('.generator-carousel .slick-current.slick-center');
    var count = $('.aside-footer .count').text().replace('шт', '').trim()
    for (var product of activeProducts) {
        DictOrderInfo[product.getAttribute('data-product-id')] = product
                .getAttribute('data-product-color')
    }
    $this.find("input[name='order_info']").val(JSON.stringify(DictOrderInfo))
    $this.find("input[name='product_count']").val(Number(count))
    $this.find('button').attr('disabled', true);
    $('.animation-loader').css('color', 'transparent').children('img').css('display', 'block')

    ajaxOrderProductSets(url, $this);
});

// Скрипт для вывода изображений разных цветов
var kitsColorForm = $(".kits-color-form");
var csrfTokenColorKits = $(".kits-color-form input[name='csrfmiddlewaretoken']").val();
var kitsColorUrl = kitsColorForm.attr('action');


$('.kits-color-form .filter-input-checkbox').on('change', function () {
    let inputColorId = $(this).val();

    $.ajax({
        url: kitsColorUrl,
        method: 'POST',
        data: {
            csrfmiddlewaretoken: csrfTokenColorKits,
            color_id: inputColorId
        },
        dataType: 'json',

        success: function (data) {
            $('.kits-flex-image').remove();
            $('.kits-flex').prepend(data.kit_images_html);
            var countSlide = $('.slider-for').children().length
            $('.slider-for').slick(sliderForSettings);
            $('.slider-nav').slick(sliderNavSettings);
            setNumberSlide();
            $('.number-slider__wrapper .all-number').text(countSlide);

        },
        error: function (error) {
            if (error.responseJSON) {
                console.log(error.responseJSON.message)
            } else {
                alert("Что-то пошло не так!")
            }
        }
    })

});

function ajaxOrderProductSets(url, element) {
    $.ajax({
        url: url,
        type: 'post',
        data: element.serialize(),

        success: function (data) {
            $('.animation-loader').css('color', '').children('img').css('display', 'none');
            $('.aside-form').css('display', 'none');
            $('.aside-success').css('display', 'block');
            element.find('.form-error-msg').text('');
            element.find('button').attr('disabled', false);
            element[0].reset();
        },

        error: function (errors) {
            $('.animation-loader').css('color', '').children('img').css('display', 'none')
            element.find('button').attr('disabled', false);
            if (errors.responseJSON) {
                element.find('.form-error-msg').text('')
                errors.responseJSON.message.forEach(function (item) {
                    element.find(`input[name='${item[0]}']`)
                            .siblings('.form-error-msg').text(item[1])
                })

            } else {
                alert('Что то пошло не так!')
            }
        }
    })
}

function productSetsLoaderTrigger(element, orderInfo, count) {
    element.find("input[name='order_info']").val(JSON.stringify(orderInfo));
    element.find("input[name='product_count']").val(Number(count));
    element.find('button').attr('disabled', true);
    $('.animation-loader').css('color', 'transparent').children('img').css('display', 'block')
}

// Скрипт для оформления заказа на конструктор форм
$("#constructor-form").on("submit", function (e) {
    e.preventDefault();
    var $this = $(this);
    var url = $this.attr('action');
    var DictOrderInfo = {};
    var activeProducts = $('.generator-carousel .slick-current.slick-center');
    var count = $('.aside-footer .count').text().replace('шт', '').trim()
    for (var product of activeProducts) {
        DictOrderInfo[product.getAttribute('data-product-id')] = product
                .getAttribute('data-product-color')
    }
    productSetsLoaderTrigger($this, DictOrderInfo, count);
    ajaxOrderProductSets(url, $this)

});

// Скрипт для оформления заказа готовых комплектов
$("#ready-made-kit-form").on("submit", function (e) {
    e.preventDefault();
    let $this = $(this);
    let madeKitUrl = $(this).attr('action');
    const defaultQuantity = 1;
    let checkedInput = $('.filter-input-checkbox:checked').val();

    let kitOrderInfo = [
        $('.aside-bar').data('product-kit-id'),
        checkedInput
    ]; // [product_id, product_color_id]
    productSetsLoaderTrigger($this, kitOrderInfo, defaultQuantity);
    ajaxOrderProductSets(madeKitUrl, $this);

});

// add like and dislike

$(document).on('click', '.like-dislike__ajax', function (e) {
    e.preventDefault()
    var likeId = $(this).data('id');
    var dislikeId = $(this).data('id')
    var url = $(this).data('url');
    var thisItem = $(this);

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            like: likeId,
            dislike: dislikeId
        },

        success: function (data) {
            if (thisItem.hasClass('product-like-action')) {
                thisItem.children(".product-like-action-count").text(data.like_count)
                thisItem.siblings('div').children(".product-like-action-count").text(data.dislikes_count)
            } else {
                thisItem.children(".product-like-action-count").text(data.dislikes_count)
                thisItem.siblings('div').children(".product-like-action-count").text(data.like_count)
            }
            thisItem.closest('.product-reviews-item-actions').find(".product-like-action-count").removeClass('active')
            if (likeId) {
                if (data.result_like === 1) {
                    thisItem.children(".product-like-action-count").addClass('active')
                } else {
                    thisItem.children(".product-like-action-count").removeClass('active')
                }
            } else {
                if (data.result_dislike === 1) {
                    thisItem.children(".product-like-action-count").addClass('active')
                } else {
                    thisItem.children(".product-like-action-count").removeClass('active')
                }
                }
            },

        error: function (errors) {
            alert("Что то пошло не так!")
        }
    })

});

// product_color_images

$('.get-image-ajax').on('change', function () {
    var productColorId = $(this).data('color-id');
    var url = $(this).data('url');
    var $this = $(this);
    $('.grid-filter-label .filter-input-checkbox').prop('checked', false);
    $('.product-detail-images__wrapper .preloader').css({
        'visibility': 'visible',
        'opacity': 1,
    });
    var heightImage = $('.product-detail-images__wrapper').height();
    $('.product-detail-images__wrapper').css('min-height', heightImage);

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            colorId: productColorId,
        },

        success: function (data) {
            $('.product-detail-images__wrapper .preloader').css({
                'visibility': 'hidden',
                'opacity': 0,
            })
            $(".product-detail-images").replaceWith(data.images_html)
            $('.product-detail-images__wrapper').css('min-height', 'auto');
            $(".product-size__wrapper .tab_container").replaceWith(data.sizes_html)
            $(".product-detail-info .product-detail-id").text(data.article)
            $(".mobile-form .tab_container").replaceWith(data.mobile_sizes_html)
            $this.find('input').prop('checked', true)
            $(".tab_container .tab_content").hide();
            $(".product-size__wrapper .grid-filter-label").first().show();
            $(".mobile-form .grid-filter-label").first().show();
            $('.modal-carousel-owl').replaceWith(data.images_html);
            $('.modal-product__carousel .product-detail-images').removeClass('product-detail-images').addClass('modal-carousel-owl')
            settingsOwl();
            if ($(window).width() <= 960) {
                setSettingsProductSlider()
            } else if ($(window).width() > 960) {
                $('.product-detail-images').owlCarousel('destroy');
            }
             hideSize();
        },

        error: function (errors) {
            $('.product-detail-images__wrapper .preloader').css({
                'visibility': 'hidden',
                'opacity': 0,
            });
            $('.product-detail-images__wrapper').css('min-height', 'auto');
            console.log('error')
        }
    })
})


// more_reviews
$(document).on('click', '.show-more-review-btn', function () {
    var page = $(this).data('page');
    var url = $(this).data('url');
    var productId = $(this).data('product-id');

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            page: page,
            productId: productId
        },

        success: function (data) {
            $('.comment-wrapper').append(data.comments_html)
            $('.show-more-review').replaceWith(data.more_reviews_html)
        },

        error: function (errors) {
            alert("Что то пошло не так!")
        }
    })

});


// delete avatar
$(document).on('click', '.delete-avatar-ajax', function () {
    var userId = $(this).data('user-id');
    var url = $(this).data('url');

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            userId: userId
        },

        success: function (data) {
            $('.profile-avatar img').attr("src", "/static/img/svg/avatar.svg")
            $('.label-avatar-btn.delete-avatar-ajax').remove();
        },
    })

});

// Скрипт добпаления товара в корзину
$('.add_to_cart').on("click", function () {
    var productId = $(this).data('id');
    var url = $(this).data('url');
    var productColor = $(this).closest('.product-info-right')
            .find('.product-color-input:checked').closest('label').data('color-id');
    var productSize = $(this).closest('.product-info-right')
            .find('.grid-filter-label:eq(0) .product-size-input:checked').parent('label');
    var productSizeArr = [];
    productSize.each(function () {
        productSizeArr.push($(this).data('size-id'))
    })

    if (productSizeArr.length > 0) {
        $('.filter-content .form-error-msg').text('')

        $.ajax({
            url: url,
            method: 'post',
            data: {
                csrfmiddlewaretoken: getCookie('csrftoken'),
                product_id: productId,
                color_id: productColor,
                size_id: String(productSizeArr),
            },

            success: function (data) {
                if (data.count_product > 0) {
                    $('.nav-link-control__item-total').removeClass('hide')
                } else {
                    $('.nav-link-control__item-total').addClass('hide')
                }
                $('#mCSB_2_container').children('.cart-dropdown-item').remove();
                $('#mCSB_2_container').append(data.cart_products);
                $('#mCSB_3_container').children('.cart-dropdown-item').remove();
                $('#mCSB_3_container').append(data.cart_products);
                $('.cart-total .cart-total-right').text(data.price + " " + '₽');
                $('.nav-link-control__item-total').text(data.count_product);
                $('.add-remove-product__wrapper').addClass('show');
                setTimeout(function () {
                    $('.add-remove-product__wrapper').removeClass('show');
                }, 1500)

            },

            error: function (errors) {
                $('.modal-error').show().find('.modal-title').text(errors.responseJSON.message)
            }
        })
    } else {
        $('.filter-content .form-error-msg').text('Выберите размер')
    }
});

function changePriceInCart(data) {
    $('.cart-right .cart-total-head div:eq(1)').text(data.price.price_with_discount + ' ₽')
    $('.cart-right .cart-total-info div:eq(1)').text(data.price.price + ' ₽')
    $('.cart-right .cart-total-info div:eq(3)').text(data.price.price_discount + ' ₽')
    $('.cart-right .cart-total-info div:eq(7)').text(data.price.count_product + ' шт')
    $('.cart-right .cart-total-info div:eq(5)').text(data.price.promo_code_discount + ' %' + ' ' + data.price.promo_code_price + ' ₽')
    $('.cart-right .cart-total-info div:eq(9)').text(data.price.delivery_price + ' ₽')
    $('.nav-link-control__item-total').text(data.price.count_product)
}

// скрипт  изменения колличеситва продукта в корзине
var countProductUrl = $('.cart-list').data('url');
var timeout = null;
$('.minus-plus__but').click(function () {
    var $this = $(this)
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(function () {
        var productId = $this.closest('.cart-item').data('product-card-id')
        var count = $this.siblings('.cart-count-input').val()
        var sizeId = $this.closest('.cart-item').data('size-id')
        var deliveryId = 'pickup';
        if ($("#val2").prop('checked')) {
            deliveryId = $('.order-time-delivery').val()
        }

        $.ajax({
            url: countProductUrl,
            method: 'post',
            data: {
                csrfmiddlewaretoken: getCookie('csrftoken'),
                product_id: productId,
                size_id: sizeId,
                count: count,
                delivery_id: deliveryId,
            },

            success: function (data) {
                changePriceInCart(data);
                $('.cart-right .cart-item').remove();
            },

            error: function (error) {
                console.log(error)
            }
        })

    }, 400)
})

//  Скрипт для удаления продукта из корзины
var deleteProductUrl = $('.cart-list').data('delete-url');

$('.cart-count-remove').on("click", function () {
    var $this = $(this);
    var productId = $this.closest('.cart-item').data('product-card-id')
    var sizeId = $this.closest('.cart-item').data('size-id')
    var deliveryId = 'pickup';
    if ($("#val2").prop('checked')) {
        deliveryId = $('.order-time-delivery').val()
    }

    $.ajax({
        url: deleteProductUrl,
        method: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            product_id: productId,
            size_id: sizeId,
            delivery_id: deliveryId,
        },

        success: function (data) {
            $this.closest('.cart-item').remove();
            if (data.count_product > 0) {
                $('.nav-link-control__item-total').removeClass('hide')
            } else {
                $('.nav-link-control__item-total').addClass('hide')
            }
            $(`#mCSB_1_container .cart-dropdown-item[data-product-id=${productId}]`).remove()
            changePriceInCart(data);
        },

        error: function (error) {
            console.log(error)
        }
    })
})

//  Скрипт активации промокода
$('.cart-promocode-btn').on("click", function () {
    var $this = $(this)
    var promo_code = $('.cart-promocode-input').val()
    var url = $this.data('url')
    var deliveryId = 'pickup';
    if ($("#val2").prop('checked')) {
        deliveryId = $('.order-time-delivery').val()
    }

    $.ajax({
        url: url,
        method: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            promo_code: promo_code,
            delivery_id: deliveryId,
        },

        success: function (data) {
            $this.siblings('.form-error-msg').text('');
            changePriceInCart(data);
            $this.siblings('.cart-promocode-input').addClass('active');
            $this.siblings('.cart-promocode-input').attr('readonly', true);
            $this.css('display', 'none');
        },

        error: function (error) {
            console.log(error)
            $this.siblings('.form-error-msg').text(error.responseJSON.message)
            $this.css('display', 'flex')
        }
    })
})

//Скрипт для активаци оплаты за доставку

var urlDelivery = $('.order-date-delivery__ajax').data('url')

$(".order-time-delivery, #val2, #val1").on("change", function () {
    $("#val2").val($('.order-time-delivery option:eq(0)').val())
    var deliveryId = $(this).val();

    $.ajax({
        url: urlDelivery,
        method: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            delivery_id: deliveryId
        },

        success: function (data) {
            changePriceInCart(data);
        },

        error: function (error) {
            console.log(error)
        }
    })
})

// скрипт для оформления заказа
$('#order').on('submit', function (e) {
    e.preventDefault();
    $("#val2").val("express_delivery");
    var url = $(this).attr('action');
    var $this = $(this);
    var arrProduct = [];
    var dictProduct = {};
    dictProduct['product'] = arrProduct
    $('.cart-item').each(function (i, obj) {
        var productCartId = $(obj).data('product-card-id');
        var productId = $(obj).data('product-id');
        var colorId = $(obj).data('color-id');
        var sizeId = $(obj).data('size-id');
        arrProduct.push([productCartId, productId, colorId, sizeId])
    })
    $("input[name='product_id']").val(JSON.stringify(dictProduct))
    $this.find('.cart-btn').attr('disabled', true).css('color', 'transparent')
            .children('img').css('display', 'block');

    $.ajax({
        url: url,
        method: 'post',
        data: $(this).serialize(),
        success: function (data) {
            $this.find('.cart-btn').attr('disabled', false).css('color', '#ffffff')
                    .children('img').css('display', 'none');
            if (data.online_payment === false) {
                $('.cart-list').children().remove();
                $('#mCSB_1_container').children().remove();
                $this.find('.form-error-msg').text('');
                $this[0].reset();
                $('.cart-right .cart-total-head div:eq(1)').text(0 + ' ₽');
                $('.cart-right .cart-total-info div:eq(1)').text(0 + ' ₽');
                $('.cart-right .cart-total-info div:eq(3)').text(0 + ' ₽');
                $('.cart-right .cart-total-info div:eq(7)').text(0 + ' шт');
                $('.cart-right .cart-total-info div:eq(5)').text(0 + ' %' + ' ' + 0 + ' ₽');
                $('.cart-right .cart-total-info div:eq(9)').text(0 + ' ₽');
                $('.cart-promocode .cart-promocode-input').removeClass('active');
                $('.cart-promocode .cart-promocode-input').attr('readonly', false);
                $('.cart-promocode .cart-promocode-input').val('');
                $('.cart-promocode .cart-promocode-btn').css('display', 'flex');
                $('.nav-link-control__item-total').text(0)
            } else {
                $('.modal-success .link-for-response').attr('href', data.response.PaymentURL)
                        .text(data.response.PaymentURL)
            }
            $('.modal-success').show().find('.modal-title').text(data.message);
        },

        error: function (error) {
            $this.find('.cart-btn').attr('disabled', false).css('color', '#ffffff')
                    .children('img').css('display', 'none');
            $this.find('.form-error-msg').text('');
            if (error.responseJSON.message) {
                error.responseJSON.message.forEach(function (item) {
                    $this.find(`input[name='${item[0]}']`)
                            .siblings('.form-error-msg').text(item[1][0])
                    if (item[0] === "type_payment") {
                        $('.modal-error').show().find('.modal-title').text(item[1][0]);
                    }
                })
            } else {
                console.log(error)
            }
        }
    })
})

// скрипт для подсчета стоимости возврата товаров
var timeoutReturn = null;
var productIdReturn = new Set()
$('.return-check').on("change", function () {
    var $this = $(this);
    var url = $this.data('url');
    var OrderId = $this.closest('.return-wrap').data('order-id');
    if (timeoutReturn) clearTimeout(timeoutReturn);
    timeoutReturn = setTimeout(function () {
        if ($this.is(":checked")) {
            productIdReturn.add($this.closest('.return-product-wrap').data('product-id'))
        } else {
            productIdReturn.delete($this.closest('.return-product-wrap').data('product-id'))
        }
        var productIdListReturn = Array.from(productIdReturn);

        $.ajax({
            url: url,
            method: 'post',
            data: {
                csrfmiddlewaretoken: getCookie('csrftoken'),
                product_id: String(productIdListReturn),
                order_id: OrderId,
            },

            success: function (data) {
                $('.return-price').text(`${data.calc.full_price} ₽`);
                $('.return-count').text(`${data.calc.full_count} шт`);
                $('.return-promocode').text(
                        `-${data.calc.promocode_discount} % (
                        ${data.calc.promocode_price} ₽)`
                );
                $('.return-sum').text(`${data.calc.price} ₽`);
            },

            error: function (error) {
                console.log(error)
            }
        })

    }, 400)
})

// скрипт для оформления возврата заказа
$('.return-foot-form').on("submit", function (e) {
    e.preventDefault();
    var $this = $(this);
    var url = $this.data('url');
    var product_list = [];
    for (var product of $('.return-check:checked')) {
        product_list.push(
                $(product).closest('.return-product-wrap').data('product-id')
        )
    }
    $("input[name='product_list']").val(product_list)
    $("input[name='order_id']").val($('.return-wrap').data('order-id'))
    $this.find('button').attr('disabled', true)
            .css('color', 'transparent').children('img')
            .css('display', 'block')

    $.ajax({
        url: url,
        method: 'post',
        data: $this.serialize(),

        success: function (data) {
            $('.form-error-msg').text('');
            $this.find('button').attr('disabled', false)
                    .css('color', '#ffffff').children('img')
                    .css('display', 'none');
            for (var product_id of product_list) {
                $(`.return-product-wrap[data-product-id='${product_id}']`).remove()
            }
            $('.return-price').text('0 ₽');
            $('.return-count').text('0 шт');
            $('.return-promocode').text('-0% (0 ₽');
            $('.return-sum').text('0 ₽');
            $this[0].reset();
            $('.modal-success').show().find('.modal-title').text(data.message);
            if ($('.return-product-wrap').length < 1) {
                $('.modal-success .modal-btn').on('click', function () {
                    window.location.replace(data.url)
                })
            }
        },

        error: function (error) {
            console.log(error.responseJSON.message)
            $this.find('button').attr('disabled', false)
                    .css('color', '#ffffff').children('img')
                    .css('display', 'none')
            if (error.responseJSON.message) {
                error.responseJSON.message.forEach(function (item) {
                    $this.find(`input[name='${item[0]}']`)
                            .next('.form-error-msg').text(item[1])
                })
            } else {
                console.log(error)
            }
        }
    })

});

$('.form-input.cart-region').on('keyup', function () {
    var url = $(this).data('url');
    var value = $(this).val();
    var $this = $(this);
    $this.siblings('.loader-img').css('display', 'block')

    if (value.length > 2) {
        $.ajax({
            url: url,
            type: 'post',
            data: {
                csrfmiddlewaretoken: getCookie('csrftoken'),
                value: value
            },
            success: function (data) {
                $this.siblings('.form-error-msg').text()
                $('#mCSB_1_container').children().remove()
                $('#mCSB_1_container').append(data.cities)
                $this.siblings('.loader-img').css('display', 'none')
            },
            error: function (error) {
                $this.siblings('.form-error-msg').text(error.responseJSON.message)
                $this.siblings('.loader-img').css('display', 'none')
            }
        })
    } else {
        $this.siblings('.loader-img').css('display', 'none')
        $('#mCSB_1_container').children().remove()
        $('#mCSB_1_container').append("<li>Нужно больше символов</li>")
    }
});

$("#add-comment-to-product").submit(function (e) {
    e.preventDefault()
    var url = $(this).attr('action');
    var productId = $(this).data('product-id')
    var userId = $(this).data('user-id')
    var $this = $(this)

    $.ajax({
        url: url,
        type: 'post',
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            productId: productId,
            rating: $("#rating").val(),
            description: $("#description").val(),
            serializer: $this.serialize()
        },

        success: function (data) {
            if ($(".product-reviews-item").length > 5) {
                $(".comment-wrapper").prepend(data.comment_html);
            } else {
                $(".comment-wrapper").prepend(data.comment_html);
                $(".product-reviews-item:nth-child(6)").remove();
            }
            $("#description").val('');
            $(".br-widget").children(':not(:first)').removeClass('br-selected');
        },
        error: function (error) {
            alert('Что то пошло не так!')
        }
    })
});

//
//-----------------FORM-----------------
//
var inputPassword = $('.js-input-type');
var btnPassword = $('.btn-toggle-type');

btnPassword.on('click', function () {
    inputPassword.closest('.form-box .form-input').toggleClass('show');
    if (inputPassword.hasClass('show')) {
        inputPassword.attr('type', 'text');
    } else {
        inputPassword.attr('type', 'password');
    }
})


$(document).ready(function () {
    $('.filter-item:first-child ').addClass('show')
})

$('.filter-tab').click(function () {
    $(this).parent('.filter-item ').toggleClass('show')
})

var cartModal = $(".wrapper-toggle");
var profileModal = $(".wrapper-toggle-user");
var cartModalBtn = $(".btn-toggle-user");
var profileModalBtn = $(".btn-toggle");

$(function () {
    profileModalBtn.on("click", function (e) {
        cartModal.toggleClass("wide");
        profileModal.removeClass("wide");
    });
});

$(function () {
    cartModalBtn.on("click", function (e) {
        profileModal.toggleClass("wide");
        cartModal.removeClass("wide");
    });
});

$(document).on('click', function (e) {
    if (
            !cartModal.is(e.target) && cartModal.has(e.target).length === 0 &&
            !profileModal.is(e.target) && profileModal.has(e.target).length === 0 &&
            !cartModalBtn.is(e.target) && cartModalBtn.has(e.target).length === 0 &&
            !profileModalBtn.is(e.target) && profileModalBtn.has(e.target).length === 0
    ) {
        cartModal.removeClass("wide");
        profileModal.removeClass("wide");
    }
})

$(function () {
    $(".btn-toggle-search , .search-dropdown-close").on("click", function (e) {
        $(".search-dropdown").toggleClass("wide");
        $(".wrapper-toggle").removeClass("wide");
    });
    $('.close-search-mobile').on('click', function () {
        $(".search-dropdown").removeClass("wide");
    })
});


$(function () {
    $(".filter-close-mobile, .close-filter-mobile").on("click", function (e) {
        $(".aside-left").removeClass("show");
        $('body').css("overflow", "auto");
    });
});
$(function () {
    $(".mobile-filter").on("click", function (e) {
        $(".aside-left").addClass("show");
        $('body').css("overflow", "hidden");
    });
});


$(".product-reviews-textarea-box").hide();

$(function () {
    $(".product-reviews-show-textarea").on("click", function (e) {
        $(".product-reviews-textarea-box").show();
    });
});
$(function () {
    $(".product-reviews-textarea-btn").on("click", function (e) {
        $(".product-reviews-textarea-box").hide();
    });
});

function setTransformNavigation() {
    var lastScroll;
    var heightWindow = $(window).height();
    var footerTop = $('footer.footer').offset().top;
    var stopTranslate = footerTop - heightWindow - 87;
    $(window).scroll(function () {
        cartModal.removeClass("wide");
        profileModal.removeClass("wide");
        if ($(this).scrollTop() >= stopTranslate) {
            $('nav.nav').css('transform', 'none')
        } else {
            if (lastScroll >= $(this).scrollTop()) {
                $('nav.nav').css('transform', 'none')
            } else {
                $('nav.nav').css('transform', 'translateY(calc(100% + 20px))')
            }
        }

        if ($(this).scrollTop() < 0) {
            lastScroll = 0
        } else {
            lastScroll = $(this).scrollTop()
        }
    });
}

$(function () {
    setTransformNavigation()
})

$(window).resize(function () {
    setTransformNavigation()
})

$('.nav-form input').on("keyup", function () {
    if ($(this).val().length > 0) {
        $(".search-result").slideDown(200)
    } else {
        $(".search-result").slideUp(200)
    }
})
$('.search-dropdown input').on("keyup", function () {
    if ($(this).val().length > 0) {
        $(".search-dropdown-results").slideDown(200)
    } else {
        $(".search-dropdown-results").slideUp(200)
    }
})

$('.modal .modal-btn').on("click", function () {
    $('.modal').hide()
})

$('.table-size').on('click', function () {
    $('.modal-table').addClass('show');
    $('body').css('overflow', 'hidden')
})

$('.close-table-size').on('click', function () {
    $('.modal-table').removeClass('show');
    $('body').css('overflow', 'auto')
})

var lastScrollBlock;

$('.burger-nav .level-nav').on("scroll", function () {
    if (lastScrollBlock >= $(this).scrollTop()) {
        $('.burger-nav .category-link').css('transform', 'none')
    } else {
        $('.burger-nav .category-link').css('transform', 'translateY(150%)')
    }

    if ($(this).scrollTop() < 0) {
        lastScrollBlock = 0
    } else {
        lastScrollBlock = $(this).scrollTop()
    }
})

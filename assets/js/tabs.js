$(window).on("load", function () {
    /* if in tab mode */
    $("ul.tabs li").click(function () {
        $(".tab_container .tab_content").hide();
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).fadeIn();
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");

    });
    $(".tab_container .tab_content").hide();
    $('.product-size__wrapper ul.tabs li').first().addClass("active");
    $(".product-size__wrapper .grid-filter-label").first().show();
    $('.product-size__wrapper ul.tabs li').last().addClass("tab_last");

    /* mobile */
    $('.filter-content ul.tabs li').first().addClass("active");
    $(".filter-content .grid-filter-label").first().show();
    $('.filter-content ul.tabs li').last().addClass("tab_last");
});

function hideSize() {
    for (var size of $('.grid-filter-label.tab_content')) {
        if (size.children.length <= 0) {
            $(`.choice-table-size .table-size-item[data-id='${size.dataset.id}']`).css('display', 'none')
        } else {
            $(`.choice-table-size .table-size-item[data-id='${size.dataset.id}']`).css('display', 'block')
        }
    }
}

hideSize();

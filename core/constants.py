MALE = 'male'
FEMALE = 'female'

GENDER_TYPE = (
    (MALE, 'Мужской'),
    (FEMALE, 'Женский')
)

MAN = 'man'
WOMAN = 'woman'
CHILDREN = 'children'
SALE = 'sale'

MAIN_CATEGORY_TYPE = (
    (MAN, 'Мужское'),
    (WOMAN, 'Женское'),
    (CHILDREN, 'Дети'),
    (SALE, 'Распродажа')
)

KIND_OF_SPORTS = 'kind_of_sports'
CLOTHES = 'clothes'
SHOES = 'shoes'
ACCESSORIES = 'accessories'
SPORTS_NUTRITION = 'sports_nutrition'

CATEGORY_TYPE = (
    (KIND_OF_SPORTS, 'Виды спорта'),
    (CLOTHES, 'Одежда'),
    (SHOES, 'Обувь'),
    (ACCESSORIES, 'Аксессуары'),
    (SPORTS_NUTRITION, 'Спортивное питание')
)


CONSTRUCTOR_ONE = 'constructor_one'
CONSTRUCTOR_TWO = 'constructor_two'
CONSTRUCTOR_THREE = 'constructor_three'
READY_MADE_KIT = 'ready_made_kit'

CONSTRUCTOR_TYPE = (
    (CONSTRUCTOR_ONE, 'Конструктор №1'),
    (CONSTRUCTOR_TWO, 'Конструктор №2'),
    (CONSTRUCTOR_THREE, 'Конструктор №3'),
    (READY_MADE_KIT, 'Готовый комплект')
)

SPECIAL_OFFER_PAGE = "special_offer"
READY_MADE_KIT_PAGE = "ready_made_kit"
COMMAND_CATEGORY_PAGE = "command_category"

BANNER_PAGES = (
    (SPECIAL_OFFER_PAGE, "Специальные предложения"),
    (READY_MADE_KIT_PAGE, "Готовые комплекты"),
    (COMMAND_CATEGORY_PAGE, "Категория для команд")
)

ORDER_STATUSES = (
    ('progress', 'Заказ обрабатывается'),
    ('delivered', 'Доставлено в пункт выдачи заказа'),
    ('received', 'Заказ получен'),
)

RETURN_ORDER_STATUSES = (
    ('return', 'Возврат'),
)

TYPE_PAYMENT = (
    ('online_payment', 'Онлайн оплата'),
    ('payment_on_delivery', 'Оплата при получении'),
)

TYPE_DELIVERY = (
    ('pickup', 'Самовывоз'),
    ('express_delivery', 'Курьерская доставка'),
)

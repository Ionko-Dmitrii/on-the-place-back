from django.db import models

from apps.account.models import User
from apps.common.models import Product, ProductColor, ProductSize


class Cart(models.Model):
    """Модель для корзины"""

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзина'

    user = models.ForeignKey(
        to=User, verbose_name='Пользователь', related_name='cart_user',
        on_delete=models.CASCADE
    )
    total_price = models.DecimalField(
        verbose_name='Итоговая цена', max_digits=10, null=True, default=0,
        decimal_places=2
    )
    quantity = models.PositiveIntegerField(default=0, verbose_name='Колличество')
    price_without_discount = models.DecimalField(
        verbose_name='Цена без скидки', max_digits=10, null=True, default=0,
        decimal_places=2
    )
    discount_price = models.DecimalField(
        verbose_name='Скидка', max_digits=10, null=True, default=0,
        decimal_places=2
    )

    def __str__(self):
        return f'Корзина: {self.user}'


class CartItem(models.Model):
    """Модель для продукта в корзине"""

    class Meta:
        verbose_name = "Товар в корзине"
        verbose_name_plural = "Товары в корзине"

    product = models.ForeignKey(
        to=Product, verbose_name='Товар', on_delete=models.CASCADE,
        related_name='cart_products'
    )
    cart = models.ForeignKey(
        to=Cart, verbose_name='Корзина', on_delete=models.CASCADE,
        related_name='carts'
    )
    quantity = models.PositiveIntegerField(
        default=0, verbose_name='Колличество товара'
    )
    color = models.ForeignKey(
        to=ProductColor, verbose_name='Цвет товара', on_delete=models.CASCADE,
        related_name='colors')
    size = models.ForeignKey(
        to=ProductSize, verbose_name='Размер товара', on_delete=models.CASCADE,
        related_name='sizes')

    def __str__(self):
        return self.product.title


class Delivery(models.Model):
    """Модель для доставки"""

    title = models.CharField(verbose_name='Название', max_length=255)
    from_time = models.TimeField(verbose_name='Время от:')
    to_time = models.TimeField(verbose_name='Время до:')
    price = models.PositiveIntegerField(
        verbose_name='Стоимость доставки', default=0
    )

    class Meta:
        verbose_name = 'Доставка'
        verbose_name_plural = 'Доставка'

    def __str__(self):
        return self.title


class PickupPointOrder(models.Model):
    """Модель для пунктов выдачи заказов"""

    address = models.CharField(
        verbose_name='Пункт выдачи заказа', max_length=255
    )
    from_time = models.TimeField(verbose_name='Время работы с:')
    to_time = models.TimeField(verbose_name='Время работы до:')
    phone = models.CharField(verbose_name='Телефон', max_length=20)

    class Meta:
        verbose_name = 'Пункт выдачи заказа'
        verbose_name_plural = 'Пункты выдачи заказов'

    def __str__(self):
        return self.address

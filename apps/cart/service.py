import datetime
import json

from django.conf import settings
from django.db.models import (
    F, ExpressionWrapper, Sum, IntegerField
)
from django.shortcuts import get_object_or_404

from apps.cart.models import CartItem, Cart, Delivery
from apps.common.models import (
    Product, ProductColor, ProductSize, PromoCode
)
from apps.common.service import send_message_to_email
from apps.order.models import OrderItem, Order, ReturnOrder, ReturnOrderItem


class Basket:
    promo_code = 0
    cookie = None
    promo_code_code = None

    @classmethod
    def get_price_product_list(cls, request, delivery_price):
        price = 0
        price_discount = 0
        count_product = 0
        if request.user.is_authenticated:
            price_list = (
                CartItem.objects.filter(
                    cart__user=request.user
                ).annotate(price_item=ExpressionWrapper(
                    F('product__price') * F('quantity'),
                    output_field=IntegerField()
                ), price_discount_promo_code=ExpressionWrapper(
                    F('product__price') / 100
                    * (F('product__discount') + cls.promo_code) * F('quantity'),
                    output_field=IntegerField()
                ), price_discount_item=ExpressionWrapper(
                    F('product__price') / 100 * F('product__discount')
                    * F('quantity'), output_field=IntegerField()
                ),
                ).select_related('product').aggregate(
                    price=Sum('price_item') + delivery_price,
                    price_discount=Sum('price_discount_item'),
                    full_count=Sum('quantity'),
                    promo_code_price=Sum('price_item') / 100 * cls.promo_code,
                    full_price_discount=Sum('price_discount_promo_code')
                )
            )

            price = price_list['price'] if price_list['price'] else 0
            price_discount = (
                price_list['price_discount'] if
                price_list['price_discount'] else 0
                                   )
            count_product = (
                price_list['full_count'] if price_list['full_count'] else 0
            )
            promo_code_price = (
                price_list['promo_code_price'] if
                price_list['promo_code_price'] else 0
            )
            full_price_discount = (
                price_list['full_price_discount'] if
                price_list['full_price_discount'] else 0
            )

            full_price_with_discount = price - full_price_discount

        else:
            product_list = cls.get_products_from_cookie_for_render(cls.cookie)
            for product in product_list:
                price += int(product[0].price * product[3])
                price_discount += int(
                    (product[0].price / 100 * product[0].discount) * product[3]
                )
                count_product += product[3]

            promo_code_price = int(price / 100 * int(cls.promo_code))
            full_price_with_discount = int(
                (price - price_discount) - promo_code_price + delivery_price
            )
            price += delivery_price

        return dict(
            price=price,
            price_discount=price_discount,
            price_with_discount=full_price_with_discount,
            count_product=count_product,
            promo_code_price=promo_code_price,
            promo_code_discount=int(cls.promo_code),
            promo_code_code=cls.promo_code_code,
            delivery_price=delivery_price,
        )

    @staticmethod
    def set_cookie(response, key, value, days_expire=60):
        if days_expire is None:
            max_age = 365 * 24 * 60 * 60  # one year
        else:
            max_age = days_expire * 24 * 60 * 60
        expires = datetime.datetime.strftime(
            datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age),
            "%a, %d-%b-%Y %H:%M:%S GMT"
        )
        response.set_cookie(key, value, max_age=max_age, expires=expires)

        return response

    @staticmethod
    def get_products_id_from_cookie(request):

        product_list = list()

        if settings.BASKET_COOKIE_NAME in request.COOKIES:
            cookie_list = request.COOKIES[settings.BASKET_COOKIE_NAME]
            product_list = json.loads(cookie_list)

        products_list = [i['product'] for i in product_list]
        product_id_list = []
        color_id_list = []
        size_id_list = []
        count_product_list = []

        for i in products_list:
            product_id_list.append(i[0])
            color_id_list.append(i[1])
            size_id_list.append(i[2])
            count_product_list.append(i[3])

        return {
            "cookie_list": product_list,
            "products_list": products_list,
            "product_id_list": product_id_list,
            "color_id_list": color_id_list,
            "size_id_list": size_id_list,
            "count_product_list": count_product_list,
        }

    @staticmethod
    def get_products_from_cookie_for_render(cookie):
        products_list = (i['product'] for i in cookie['cookie_list'])

        html_product_list = []
        for product in products_list:
            products = get_object_or_404(
                Product,
                id=product[0],
                product_colors__id=product[1],
                product_colors__product_color__id=product[2]
            )
            colors = get_object_or_404(
                ProductColor,
                product_id=product[0], id=product[1],
                product_color__id=product[2]
            )
            size = get_object_or_404(ProductSize, id=product[2])
            html_product_list.append([products, colors, size, product[3]])

        return html_product_list

    @staticmethod
    def added_product_to_database(request, product, color, size_list):
        cart_user, _ = Cart.objects.get_or_create(user=request.user)
        total_quantity = int()
        for size in size_list:
            if cart_user.carts.filter(color_id=color.id,
                                      size_id=size.id).exists():
                product_cart = get_object_or_404(
                    CartItem, color=color, size=size
                )
                quantity = product_cart.quantity
                total_quantity = size.quantity

                if quantity > total_quantity - 1:
                    return dict(success=False, quantity=total_quantity)
                else:
                    product_cart.quantity = F('quantity') + 1
                    product_cart.save()
            else:
                cart_item = CartItem(
                    product=product,
                    cart=cart_user,
                    quantity=1,
                    color=color,
                    size=size,
                )
                cart_item.save()
        return dict(success=True, quantity=total_quantity)

    @staticmethod
    def added_product_id_to_cookie(cookie, product_id, color_id, size_list_id):
        for size in size_list_id:
            if (color_id in cookie['color_id_list']
                    and size in cookie['size_id_list']):
                index = cookie['size_id_list'].index(size)
                count = cookie['cookie_list'][index]['product'][3]

                size_title = ProductSize.objects.filter(id=size).first()
                total_quantity = size_title.quantity
                if count > total_quantity - 1:
                    return dict(success=False, quantity=total_quantity)
                else:
                    count += 1
                    cookie['cookie_list'][index]['product'][3] = count

                    return dict(success=True)
            else:
                count = 1
                cookie['cookie_list'].insert(0, (
                    {"product": [product_id, color_id, size, count]}
                ))

                return dict(success=True)


class OrderService:

    @classmethod
    def save_order_to_database(cls, request, form, product_list):
        if form.cleaned_data.get('type_delivery') == 'express_delivery':
            delivery = (
                get_object_or_404(
                    Delivery, id=form.cleaned_data.get('time_delivery')
                )
            )
            delivery_price = delivery.price
            delivery_from_time = delivery.from_time
            delivery_to_time = delivery.to_time
        else:
            delivery_from_time = None
            delivery_to_time = None
            delivery_price = 0
        promo_code = form.cleaned_data.get('promo_code_title')

        if promo_code and PromoCode.objects.filter(code=promo_code).exists():
            Basket.promo_code = get_object_or_404(
                PromoCode, code=promo_code
            ).discount
        price = Basket.get_price_product_list(request, delivery_price)
        order = form.save(commit=False)
        if request.user.is_authenticated:
            order.user = request.user
        if form.cleaned_data.get('type_delivery') == 'express_delivery':
            order.date = form.cleaned_data.get('date')
            order.time_from = delivery_from_time
            order.time_to = delivery_to_time
            order.delivery_price = delivery_price
            order.address_delivery = None
        else:
            order.address_delivery = form.cleaned_data.get('address_delivery')
        order.delivery_price = price['delivery_price']
        order.total_price = price['price']
        order.price_with_discount = price['price_with_discount']
        order.price_discount = price['price_discount']
        order.promo_code_discount_price = price['promo_code_price']
        order.promo_code_discount = price['promo_code_discount']
        order.quantity = price['count_product']
        order.save()

        order_products_list = cls.save_order_item(
            request, order, product_list
        )

        OrderItem.objects.bulk_create(order_products_list)

        return {
            'order': order,
            'order_products_list': order_products_list,
            'price': price,
        }

    @classmethod
    def save_order_item(cls, request, order, product_list):

        order_products_list = []
        for product in product_list:
            if request.user.is_authenticated:
                obj = OrderItem(
                    order=order,
                    product=product[1],
                    quantity=product[0].quantity,
                    color=product[0].color,
                    article=product[0].color.article,
                    size=product[0].size.size.eu_size,
                    title=product[1].title,
                    total_price=product[1].price,
                    price_with_discount=product[1].get_discount_sum,
                    discount=product[1].discount,
                    price_discount=product[1].price_discount,
                    image=product[2].image,
                    text=product[1].description,
                )

                size = ProductSize.objects.filter(
                    sizes__product=product[1],
                    size__eu_size=product[0].size.size.eu_size,
                    sizes__color=product[0].color
                ).first()
                size.quantity = size.quantity - product[0].quantity
                if size.quantity < 1:
                    size.is_available = False
                    size.save(update_fields=['is_available', 'quantity'])
                else:
                    size.save(update_fields=['quantity'])

            else:
                obj = OrderItem(
                    order=order,
                    product=product[0],
                    quantity=product[4],
                    color=product[1],
                    article=product[1].article,
                    size=product[2].size.eu_size,
                    title=product[0].title,
                    total_price=product[0].price,
                    price_with_discount=product[0].get_discount_sum,
                    discount=product[0].discount,
                    price_discount=product[0].price_discount,
                    image=product[3].image,
                    text=product[0].description,
                )

                size = ProductSize.objects.filter(
                    product_color__product=product[0],
                    size__eu_size=product[2].size.eu_size,
                    product_color__color__name=product[1]
                ).first()
                size.quantity = size.quantity - product[4]
                if size.quantity < 1:
                    size.is_available = False
                    size.save(update_fields=['is_available', 'quantity'])
                else:
                    size.save(update_fields=['quantity'])

            order_products_list.append(obj)

        return order_products_list

    @classmethod
    def calc_return_order(cls, product_id_list, order_id):
        promocode_discount = get_object_or_404(
            Order, id=order_id
        ).promo_code_discount
        if len(product_id_list[0]) > 0:
            price = OrderItem.objects.filter(id__in=product_id_list).annotate(
                price_all_item=ExpressionWrapper(
                    F('quantity') * F('price_with_discount'),
                    output_field=IntegerField()
                ), price_without_discount=ExpressionWrapper(
                    F('quantity') * F('total_price'),
                    output_field=IntegerField()
                )
            ).aggregate(
                full_price=Sum('price_all_item'), full_count=Sum('quantity'),
                full_price_without_discount=Sum('price_without_discount'),
                full_price_discount=Sum('price_without_discount') - Sum('price_all_item'),
            )
            promocode_price = price['full_price_without_discount'] / 100 * int(promocode_discount)
            calc = {
                'full_price': price['full_price'],
                'full_price_without_discount': price['full_price_without_discount'],
                'full_price_discount': price['full_price_discount'],
                'full_count': price['full_count'],
                'promocode_discount': int(promocode_discount),
                'promocode_price': int(promocode_price),
                'price': price['full_price'] - int(promocode_price)
            }
        else:
            calc = {
                'full_price': 0,
                'full_price_without_discount': 0,
                'full_price_discount': 0,
                'full_count': 0,
                'promocode_discount': 0,
                'promocode_price': 0,
                'price': 0,
            }

        return calc

    @staticmethod
    def save_order_return(form, order, calc, user):
        order_return = form.save(commit=False)
        order_return.user = user
        order_return.status_payment = order.status_payment
        order_return.name = order.name
        order_return.last_name = order.last_name
        order_return.second_name = order.second_name
        order_return.address = order.address
        order_return.address_delivery = order.address_delivery
        order_return.email = order.email
        order_return.time_from = order.time_from
        order_return.time_to = order.time_to
        order_return.date = order.date
        order_return.delivery_price = order.delivery_price
        order_return.id = order.id
        order_return.type_delivery = order.type_delivery
        order_return.type_payment = order.type_payment
        order_return.total_price = calc['full_price_without_discount']
        order_return.price_with_discount = calc['price']
        order_return.price_discount = calc['full_price_discount']
        order_return.promo_code_discount = calc['promocode_discount']
        order_return.promo_code_discount_price = calc['promocode_price']
        order_return.quantity = calc['full_count']

        return order_return

    @classmethod
    def update_return_order_order(cls, order_id, form, user, order, calc):
        if ReturnOrder.objects.filter(id=order_id).exists():
            order_return = get_object_or_404(ReturnOrder, id=order_id)
            order_return.total_price = (
                    order_return.total_price +
                    calc['full_price_without_discount']
            )
            order_return.price_with_discount = (
                    order_return.price_with_discount + calc['price']
            )
            order_return.price_discount = (
                    order_return.price_discount +
                    calc['full_price_discount']
            )
            order_return.promo_code_discount_price = (
                    order_return.promo_code_discount_price +
                    calc['promocode_price']
            )
            order_return.quantity = (
                    order_return.quantity + calc['full_count']
            )
            order_return.save(update_fields=[
                'total_price', 'price_with_discount', 'price_discount',
                'quantity', 'promo_code_discount_price'
            ])
        else:
            order_return = cls.save_order_return(form, order, calc, user)

        return order_return

    @staticmethod
    def save_return_order_products(order_products_list, order_return):
        return_order_products_list = []
        for return_product in order_products_list:
            obj = ReturnOrderItem(
                return_order=order_return,
                quantity=return_product.quantity,
                color=return_product.color,
                article=return_product.article,
                size=return_product.size,
                title=return_product.title,
                total_price=return_product.total_price,
                price_with_discount=return_product.price_with_discount,
                discount=return_product.discount,
                price_discount=return_product.price_discount,
                image=return_product.image,
                text=return_product.text,
            )
            return_order_products_list.append(obj)
            return_product.delete()

        ReturnOrderItem.objects.bulk_create(return_order_products_list)

    @staticmethod
    def update_old_order(order, calc):
        order.total_price = order.total_price - calc[
            'full_price_without_discount']
        order.price_with_discount = (
                order.price_with_discount - calc['price']
        )
        order.price_discount = (
                order.price_discount - calc['full_price_discount']
        )
        order.promo_code_discount_price = (
                order.promo_code_discount_price -
                calc['promocode_price'])
        order.quantity = order.quantity - calc['full_count']
        order.save(update_fields=[
            'total_price', 'price_with_discount', 'price_discount',
            'quantity', 'promo_code_discount_price'
        ])

    @classmethod
    def save_return_order_to_database(
            cls, order, order_products_list, order_id, product_list_id, form,
            user, render_template_name
    ):
        calc = OrderService.calc_return_order(product_list_id, order_id)
        order_return = cls.update_return_order_order(
            order_id, form, user, order, calc
        )

        order_return.save()

        cls.save_return_order_products(order_products_list, order_return)

        order_item_old = OrderItem.objects.filter(order=order)
        if len(order_item_old) < 1:
            get_object_or_404(Order, id=order_id).delete()
        else:
            cls.update_old_order(order, calc)

        send_message_to_email(
            cls.save_order_return(form, order, calc, user), order_products_list,
            render_template_name
        )

from django.urls import path

from apps.cart.views import (
    CartView, AddProductToCart, DeliveryTime, TinkoffSuccessView,
    TinkoffErrorView, GetRegionAndCountry
)

urlpatterns = [
    path('', CartView.as_view(), name='cart'),
    path('add-product-to-cart/', AddProductToCart.as_view(),
         name='add_product_to_cart'),
    path('delivery-time/', DeliveryTime.as_view(),
         name='delivery-time'),
    path('tinkoff-success/', TinkoffSuccessView.as_view(),
         name='tinkoff-success'),
    path('tinkoff-error/', TinkoffErrorView.as_view(), name='tinkoff-error'),
    path('country-region/', GetRegionAndCountry.as_view(), name='country-region'),
]

from django.contrib import admin

from apps.cart.models import Cart, CartItem, Delivery, PickupPointOrder


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    inlines = [CartItemInline]


@admin.register(Delivery)
class DeliveryAdmin(admin.ModelAdmin):
    list_display = ('title', 'from_time', 'to_time', 'price')


@admin.register(PickupPointOrder)
class PickupPointOrderAdmin(admin.ModelAdmin):
    list_display = ('address', 'from_time', 'to_time', 'phone')

import json
import requests

from django.utils import timezone
from django.conf import settings
from django.http import JsonResponse
from django.template import loader
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.shortcuts import get_object_or_404

from apps.cart.service import Basket
from apps.cart.models import CartItem, Delivery, PickupPointOrder
from apps.common.models import Product, ProductColor, ProductSize, PromoCode


class CartView(TemplateView):
    """View для корзины"""

    template_name = 'pages/cart.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        if request.user.is_authenticated:
            context['product_list'] = CartItem.objects.filter(
                cart__user=request.user
            ).order_by('-id')
        else:
            cookie = Basket.get_products_id_from_cookie(request)
            context['product_list'] = (
                Basket.get_products_from_cookie_for_render(cookie)
            )
        context['delivery_list'] = Delivery.objects.all()
        context['pick_up_points_list'] = PickupPointOrder.objects.all()

        return self.render_to_response(context)


class AddProductToCart(View):
    """View для добавления товара в корзину"""
    render_template_name = 'components/product-nav-cart.html'

    def post(self, request, *args, **kwargs):
        product_id = request.POST.get('product_id')
        color_id = request.POST.get('color_id')
        size_list_id = request.POST.get('size_id').split(',')
        product = Product.objects.get(id=request.POST.get('product_id'))
        color = ProductColor.objects.get(id=request.POST.get('color_id'))
        size_list = ProductSize.objects.filter(
            id__in=request.POST.get('size_id').split(',')
        )
        delivery_price = 0

        if request.user.is_authenticated:
            add_product = Basket.added_product_to_database(
                request, product, color, size_list
            )

            if not add_product['success']:

                return JsonResponse(dict(
                    success=False,
                    message=f"Превышенно допустимое колличество, доступно {add_product['quantity']}"
                ), status=400)

            else:
                price = (
                    Basket.get_price_product_list(request, delivery_price)
                )
                products = CartItem.objects.filter(
                    cart__user=request.user
                ).order_by('-id')
                products_html = loader.render_to_string(
                    self.render_template_name, {
                        'get_products_cart': products,
                        'user_cookie': request.user.is_authenticated
                    }
                )

                return JsonResponse(dict(
                    cart_products=products_html,
                    price=price['price_with_discount'],
                    count_product=price['count_product'],
                ), status=200)

        else:
            cookie = Basket.get_products_id_from_cookie(request)
            Basket.cookie = cookie
            product = Basket.added_product_id_to_cookie(
                cookie, product_id, color_id, size_list_id
            )
            if not product['success']:

                return JsonResponse(dict(
                    success=False,
                    message=f"Превышенно допустимое колличество, доступно {product['quantity']}"
                ), status=400)

            else:
                html_product_list = Basket.get_products_from_cookie_for_render(
                    cookie)
                products_html = loader.render_to_string(
                    self.render_template_name, {
                        'get_products_cart': html_product_list,
                        'user_cookie': request.user.is_authenticated
                    }
                )
                price = (
                    Basket.get_price_product_list(request, delivery_price)
                )

                response = JsonResponse(dict(
                    cart_products=products_html,
                    price=price['price_with_discount'],
                    count_product=price['count_product'],
                ), status=200)
                Basket.set_cookie(response, settings.BASKET_COOKIE_NAME,
                                  json.dumps(cookie['cookie_list']))

                return response


class ChangeCountProduct(View):
    """View для изменения колличества продуктов"""

    def post(self, request, *args, **kwargs):
        product_id = request.POST.get('product_id')
        size_id = request.POST.get('size_id')
        count = request.POST.get('count')
        delivery_id = request.POST.get('delivery_id')
        if delivery_id == 'pickup':
            delivery_price = 0
        else:
            delivery_price = get_object_or_404(Delivery, id=delivery_id).price

        if request.user.is_authenticated:
            cart_item = get_object_or_404(CartItem, id=product_id)
            available_count_product = get_object_or_404(
                ProductSize, id=size_id).quantity
            if int(count) <= available_count_product:
                cart_item.quantity = count
                cart_item.save(update_fields=['quantity'])
                price = Basket.get_price_product_list(request, delivery_price)

                return JsonResponse(dict(
                    success=True,
                    price=price,
                ), status=200)

            return JsonResponse(dict(
                success=False,
                message='Превышенно допустимое колличество товара',
            ), status=400)
        else:
            cookie = Basket.get_products_id_from_cookie(request)
            Basket.cookie = cookie
            index = cookie['size_id_list'].index(size_id)
            cookie['cookie_list'][index]['product'][3] = int(count)
            price = Basket.get_price_product_list(request, delivery_price)

            response = JsonResponse(dict(success=True, price=price), status=200)
            Basket.set_cookie(response, settings.BASKET_COOKIE_NAME,
                              json.dumps(cookie['cookie_list']))
            return response


class DeleteProductFromCart(View):
    """View для удаление продукта"""

    def post(self, request, *args, **kwargs):
        product_id = request.POST.get('product_id')
        delivery_id = request.POST.get('delivery_id')
        if delivery_id == 'pickup':
            delivery_price = 0
        else:
            delivery_price = get_object_or_404(Delivery, id=delivery_id).price

        if request.user.is_authenticated:
            cart_item = get_object_or_404(CartItem, id=product_id)
            cart_item.delete()
            price = Basket.get_price_product_list(request, delivery_price)

            return JsonResponse(dict(success=True, price=price), status=200)
        else:
            size_id = request.POST.get('size_id')
            cookie = Basket.get_products_id_from_cookie(request)
            Basket.cookie = cookie
            index = cookie['size_id_list'].index(size_id)
            cookie['cookie_list'].pop(index)
            price = Basket.get_price_product_list(request, delivery_price)
            response = JsonResponse(dict(success=True, price=price), status=200)
            Basket.set_cookie(response, settings.BASKET_COOKIE_NAME,
                              json.dumps(cookie['cookie_list']))

            return response


class ActivatePromoCode(View):
    """View для активации промокода"""

    def post(self, request, *args, **kwargs):
        promo_code_code = request.POST.get('promo_code')
        promo_code_obj_list = PromoCode.objects.filter(code=promo_code_code)
        delivery_id = request.POST.get('delivery_id')
        if delivery_id == 'pickup':
            delivery_price = 0
        else:
            delivery_price = get_object_or_404(Delivery, id=delivery_id).price

        if promo_code_obj_list.exists():
            promo_code_obj = promo_code_obj_list.first()
            quantity = promo_code_obj.quantity
            today_date = timezone.now()
            end_date = promo_code_obj.time_end
            if quantity and today_date < end_date:
                Basket.promo_code = promo_code_obj.discount
                Basket.promo_code_code = promo_code_code
                promo_code_obj.quantity = quantity - 1
                promo_code_obj.save()
                price = Basket.get_price_product_list(request, delivery_price)

                return JsonResponse(dict(success=True, price=price), status=200)
            else:

                return JsonResponse(dict(
                    success=False,
                    message='Лимит или время активации промокода превышен!'
                ), status=400)

        return JsonResponse(dict(
            success=False, message='Данный промокод отсутствует!'
        ), status=400)


class DeliveryTime(View):
    """View для выбора времени и суммы доставки"""

    def post(self, request, *args, **kwargs):
        delivery_id = request.POST.get('delivery_id')
        if delivery_id == 'pickup':
            delivery_price = 0
        else:
            delivery_price = get_object_or_404(Delivery, id=delivery_id).price
        price = Basket.get_price_product_list(request, delivery_price)

        return JsonResponse(dict(success=True, price=price), status=200)


class TinkoffSuccessView(TemplateView):
    template_name = 'pages/tinkoff-success.html'


class TinkoffErrorView(TemplateView):
    template_name = 'pages/tinkoff-error.html'


class GetRegionAndCountry(View):
    """View для рендерига городов"""
    render_template = 'components/render-cities.html'

    def post(self, request, *args, **kwargs):
        region = request.POST.get('value')
        params = {
            'lang': '0',
            'user_ids': settings.ID_USER_VK,
            'access_token': settings.TOKEN_VK,
            'country_id': '1',
            'code': 'RU',
            'q': region,
            'v': '5.131'
        }
        resp = requests.get(
            url=settings.LINK_COUNTRIES_VK,
            params=params
                            )

        if list(resp.json().keys())[0] == 'response':
            cities = ([city.get('title'), city.get('area'), city.get('region')]
                      for city in resp.json()['response']['items'])
            cities_html = loader.render_to_string(self.render_template, {
                'cities': cities,
                'value': region
            })

            return JsonResponse(
                dict(succes=True, cities=cities_html), status=200
            )

        else:
            return JsonResponse(
                dict(succes=False, message=resp.json()['error']['error_msg']),
                status=401
            )

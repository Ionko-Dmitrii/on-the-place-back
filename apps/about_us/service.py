from datetime import datetime

from django.shortcuts import redirect

from apps.about_us.form import PartnershipApplicationForm
from apps.about_us.models import FormLandlord, QuestionnaireForPartner, \
    PartnershipApplication


class FormAboutUsService:

    @staticmethod
    def get_landlords_form(form):
        if form.is_valid():
            landlord_form = form.save(commit=False)
            FormLandlord(
                address=form.cleaned_data['address'],
                total_area=form.cleaned_data['total_area'],
                rent_rate=form.cleaned_data['rent_rate'],
                name_company=form.cleaned_data['name_company'],
                the_contact_person=form.cleaned_data['the_contact_person'],
                email=form.cleaned_data['email'],
                phone_number=form.cleaned_data['phone_number'],
                comment=form.cleaned_data['comment'],
                date=datetime.now
            )
            landlord_form.save()
        return redirect('landlords')

    @staticmethod
    def questionnaire_for_partner(form):
        if form.is_valid():
            questionnaire_partner = form.save(commit=False)
            QuestionnaireForPartner(
                name=form.cleaned_data['name'],
                surname=form.cleaned_data['surname'],
                email=form.cleaned_data['email'],
                phone_number=form.cleaned_data['phone_number'],
                product_category=form.cleaned_data['product_category'],
                name_company=form.cleaned_data['name_company'],
                brand_name=form.cleaned_data['brand_name'],
                link_to_website=form.cleaned_data['link_to_website'],
                your_appeal=form.cleaned_data['your_appeal']
            )
            questionnaire_partner.save()
        return redirect('connect-marketplace')

    @staticmethod
    def get_partnership_application(form):
        if form.is_valid():
            partnership_application = form.save(commit=False)
            PartnershipApplication(
                name_company=form.cleaned_data['name_company'],
                name=form.cleaned_data['name'],
                surname=form.cleaned_data['surname'],
                email=form.cleaned_data['email'],
                comment=form.cleaned_data['comment']
            )
            partnership_application.save()
        else:
            form = PartnershipApplicationForm()
        return redirect('become-contractor')

from django.urls import path

from apps.about_us.views import (
    AboutUsView, BecomeContractorView, ConnectMarketplaceView, LandlordsView,
    NewsView, NewsDetailView, HelpView, DeliveryView, TypePaymentView
)

urlpatterns = [
    path('become-contractor/', BecomeContractorView.as_view(),
         name='become-contractor'),
    path('connect-marketplace/', ConnectMarketplaceView.as_view(),
         name='connect-marketplace'),
    path('landlords/', LandlordsView.as_view(), name='landlords'),
    path('news', NewsView.as_view(), name='news'),
    path('news-detail/<slug:slug>/', NewsDetailView.as_view(),
         name='news-detail'),
    path('help/', HelpView.as_view(), name='help'),
    path('delivery/', DeliveryView.as_view(), name='delivery'),
    path('type-payment/', TypePaymentView.as_view(), name='type-payment'),
    path('', AboutUsView.as_view(), name='about-us'),
]

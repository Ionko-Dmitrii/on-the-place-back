# Generated by Django 2.2 on 2021-07-30 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0008_constructorformapplication'),
    ]

    operations = [
        migrations.AlterField(
            model_name='constructorformapplication',
            name='text',
            field=models.TextField(blank=True, null=True, verbose_name='Коментарии'),
        ),
    ]

# Generated by Django 2.2 on 2021-08-12 08:33

import core.utils
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0012_formlandlord_partnershipapplication_questionnaireforpartner'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='news_image',
            field=models.ImageField(blank=True, null=True, upload_to=core.utils.generate_filename, verbose_name='Картинка для новости'),
        ),
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=core.utils.generate_filename, verbose_name='Изображение для слайдера на главной странице'),
        ),
    ]

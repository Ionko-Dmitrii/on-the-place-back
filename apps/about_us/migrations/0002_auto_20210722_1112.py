# Generated by Django 2.2 on 2021-07-22 05:12

import core.utils
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BecomeContractor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_in_paid', models.CharField(blank=True, help_text='Описание в картинке', max_length=255, verbose_name='Описание в картинке')),
                ('image_background', models.ImageField(upload_to=core.utils.generate_filename, verbose_name='Изображение')),
                ('title', models.CharField(max_length=255, verbose_name='Описание')),
                ('description', models.TextField(verbose_name='Текст')),
                ('title_in_paid_footer', models.CharField(blank=True, help_text='Описание во второй картине', max_length=255, verbose_name='Описание во второй картине')),
                ('image_background_footer', models.ImageField(upload_to=core.utils.generate_filename, verbose_name='Второе изображение')),
            ],
            options={
                'verbose_name': 'Стать подрядчиком ON THE PLACE',
                'verbose_name_plural': 'Стать подрядчиком ON THE PLACE',
            },
        ),
        migrations.CreateModel(
            name='ConnectPartnership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_in_paid', models.CharField(blank=True, help_text='Описание в картинке', max_length=255, verbose_name='Описание в картинке')),
                ('image_background', models.ImageField(upload_to=core.utils.generate_filename, verbose_name='Изображение')),
                ('title', models.CharField(max_length=255, verbose_name='Описание')),
                ('description', models.TextField(verbose_name='Текст')),
                ('title_in_page', models.CharField(help_text='Описание №2', max_length=255, verbose_name='Описание №2')),
                ('description_in_page', models.TextField(verbose_name='Текст №2')),
                ('title_in_footer', models.CharField(help_text='Описание №3', max_length=255, verbose_name='Описание №3')),
                ('description_in_footer', models.TextField(verbose_name='Текст №3')),
            ],
            options={
                'verbose_name': 'Подключиться к партнерской программе “Маркетплейс”',
                'verbose_name_plural': 'Подключиться к партнерской программе “Маркетплейс”',
            },
        ),
        migrations.CreateModel(
            name='Rent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_in_paid', models.CharField(blank=True, help_text='Описание в картинке', max_length=255, verbose_name='Описание в картинке')),
                ('image_background', models.ImageField(upload_to=core.utils.generate_filename, verbose_name='Изображение')),
                ('title', models.CharField(max_length=255, verbose_name='Описание')),
                ('description', models.TextField(verbose_name='Текст')),
            ],
            options={
                'verbose_name': 'Арендодателям',
                'verbose_name_plural': 'Арендодателям',
            },
        ),
        migrations.AlterModelOptions(
            name='aboutuse',
            options={'verbose_name': 'О нас', 'verbose_name_plural': 'О нас'},
        ),
        migrations.AlterModelOptions(
            name='ouradvantages',
            options={'verbose_name': 'Наши преимущества', 'verbose_name_plural': 'Наши преимущества'},
        ),
        migrations.RemoveField(
            model_name='aboutuse',
            name='title_two',
        ),
        migrations.AddField(
            model_name='aboutuse',
            name='title_in_paid',
            field=models.CharField(blank=True, help_text='Описание в картинке', max_length=255, verbose_name='Описание в картинке'),
        ),
        migrations.AlterField(
            model_name='aboutuse',
            name='small_description',
            field=models.TextField(blank=True, help_text='Текст в картинке', verbose_name='Текст в картинке'),
        ),
        migrations.AlterField(
            model_name='aboutuse',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Описание'),
        ),
        migrations.CreateModel(
            name='HowStart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Описание')),
                ('description', models.TextField(verbose_name='Текст')),
                ('connect_partnership', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='how_start', to='about_us.ConnectPartnership')),
            ],
            options={
                'verbose_name': 'Как начать',
                'verbose_name_plural': 'Как начать',
            },
        ),
        migrations.CreateModel(
            name='ClientGet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Описание')),
                ('description', models.TextField(verbose_name='Текст')),
                ('image', models.ImageField(upload_to=core.utils.generate_filename, verbose_name='Изображение')),
                ('connect_partnership', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='client_get', to='about_us.ConnectPartnership')),
            ],
            options={
                'verbose_name': 'Что вы получите',
                'verbose_name_plural': 'Что вы получите',
            },
        ),
    ]

# Generated by Django 2.2 on 2021-09-16 10:00

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0014_auto_20210915_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questionnaireforpartner',
            name='phone_number',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None, verbose_name='Телефон'),
        ),
    ]

from django import forms

from apps.about_us.models import (
    FormLandlord, PartnershipApplication, QuestionnaireForPartner
)


class LandlordForm(forms.ModelForm):

    class Meta:
        model = FormLandlord
        fields = (
            'address', 'total_area', 'rent_rate', 'name_company',
            'the_contact_person', 'email', 'phone_number', 'comment'
        )


class PartnershipApplicationForm(forms.ModelForm):

    class Meta:
        model = PartnershipApplication
        fields = (
            'name_company', 'name', 'surname', 'email', 'comment'
        )


class QuestionnaireForPartnerForm(forms.ModelForm):

    class Meta:
        model = QuestionnaireForPartner
        fields = (
            'name', 'surname', 'email', 'phone_number', 'name_company',
            'product_category', 'brand_name', 'link_to_website', 'your_appeal'
        )

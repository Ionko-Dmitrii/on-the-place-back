from django.contrib import admin

from solo.admin import SingletonModelAdmin

from apps.about_us.models import (
    OurAdvantages, AboutUs, Rent, BecomeContractor, FormLandlord,
    PartnershipApplication, QuestionnaireForPartner,
    ConnectPartnership, ClientGet, HowStart, News, Contact, Help, Delivery,
    TypePayment
)


@admin.register(OurAdvantages)
class OurAdvantagesAdmin(admin.ModelAdmin):
    fields = (
        'category', 'title', 'description', 'number',
    )
    list_display = ('title',)


@admin.register(AboutUs)
class AboutUseAdmin(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(Rent)
class RentAdmin(admin.ModelAdmin):
    list_display = ('title',)

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(RentAdmin, self).has_add_permission(request)


@admin.register(BecomeContractor)
class BecomeContractorAdmin(admin.ModelAdmin):
    fields = (
        'title', 'description', 'title_in_paid', 'image_background',
        'title_in_paid_footer', 'image_background_footer'
    )
    list_display = ('title',)


class ClientGetStackedInline(admin.StackedInline):
    model = ClientGet
    extra = 1


class HowStartStackedInline(admin.StackedInline):
    model = HowStart
    extra = 1


@admin.register(ConnectPartnership)
class ConnectPartnershipAdmin(admin.ModelAdmin):
    fields = (
        'title', 'description', 'title_in_paid', 'image_background',
        'title_in_page', 'description_in_page', 'title_in_footer',
        'description_in_footer'
    )
    list_display = ('title',)
    inlines = (HowStartStackedInline, ClientGetStackedInline)


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',), }


@admin.register(Contact)
class ContactAdmin(SingletonModelAdmin):
    list_display = ('phone', 'email')


@admin.register(Help)
class HelpAdmin(admin.ModelAdmin):
    list_display = ('title', 'description')


@admin.register(FormLandlord)
class FormLandlordAdmin(admin.ModelAdmin):
    list_display = ('name_company', 'phone_number', 'email', 'date')
    readonly_fields = (
        'address', 'total_area', 'rent_rate', 'name_company',
        'the_contact_person', 'email', 'phone_number', 'comment', 'date'
    )


@admin.register(PartnershipApplication)
class PartnershipApplicationAdmin(admin.ModelAdmin):
    list_display = ('name_company', 'email', 'date')
    readonly_fields = (
        'name_company', 'name', 'surname', 'email', 'comment', 'date'
    )


@admin.register(QuestionnaireForPartner)
class QuestionnaireForPartnerAdmin(admin.ModelAdmin):
    list_display = ('name_company', 'email', 'phone_number', 'date')
    readonly_fields = (
        'name', 'surname', 'email', 'phone_number', 'name_company',
        'product_category', 'brand_name', 'link_to_website', 'your_appeal',
        'date'
    )


@admin.register(Delivery)
class DeliveryAdmin(SingletonModelAdmin):
    pass


@admin.register(TypePayment)
class TypePaymentAdmin(SingletonModelAdmin):
    pass

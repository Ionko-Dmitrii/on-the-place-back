from django.views.generic import TemplateView, ListView, DetailView
from django.http import JsonResponse
from django.template.loader import render_to_string

from apps.about_us.form import (
    LandlordForm, PartnershipApplicationForm, QuestionnaireForPartnerForm
)
from apps.about_us.models import (
    AboutUs, OurAdvantages, ConnectPartnership, ClientGet, HowStart,
    BecomeContractor, Rent, News, Help, Delivery, TypePayment
)
from apps.about_us.service import FormAboutUsService
from apps.common.models import Category


class AboutUsView(TemplateView):
    template_name = 'pages/about-us.html'

    def get_context_data(self, **kwargs):
        context = super(AboutUsView, self).get_context_data(**kwargs)
        context['about_us'] = AboutUs.objects.first()
        context['advantages'] = OurAdvantages.objects.filter(category=1)[:3]

        return context


class BecomeContractorView(TemplateView):
    template_name = 'pages/become-contractor-ON-THE-PLACE.html'
    form_class = PartnershipApplicationForm

    def get_context_data(self, **kwargs):
        context = super(BecomeContractorView, self).get_context_data(**kwargs)
        context['become_contractor'] = BecomeContractor.objects.first()
        context['advantages'] = OurAdvantages.objects.filter(category=3)

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        return FormAboutUsService.get_partnership_application(form)


class ConnectMarketplaceView(TemplateView):
    template_name = 'pages/connect-to-the-Marketplace-partner-program.html'
    form_class = QuestionnaireForPartnerForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(ConnectMarketplaceView, self).get_context_data(**kwargs)
        context['connect_partnership'] = ConnectPartnership.objects.first()
        context['client_get'] = ClientGet.objects.all()
        context['how_start'] = HowStart.objects.all()
        context['category'] = Category.objects.all()

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        return FormAboutUsService.questionnaire_for_partner(form)


class LandlordsView(TemplateView):
    template_name = 'pages/landlords.html'
    form_class = LandlordForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(LandlordsView, self).get_context_data(**kwargs)
        context['landlords'] = Rent.objects.first()
        context['advantages'] = OurAdvantages.objects.filter(category=2)

        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        return FormAboutUsService.get_landlords_form(form)


class NewsView(ListView):
    model = News
    template_name = 'pages/profile-news.html'
    paginate_by = 5
    render_pagination_template = 'components/pagination.html'
    render_news_template = 'components/render-news.html'

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        news = context['news_list']
        left_lst, right_lst = self.left_right_division(news)
        context.update({'left_lst': left_lst, 'right_lst': right_lst})

        return context

    @staticmethod
    def left_right_division(queryset):
        news = list(queryset)
        left_lst = news[0::3]
        right_lst = [item for item in news if item not in left_lst]
        return left_lst, right_lst

    def post(self, request, *args, **kwargs):
        page_number = request.POST.get('page_number', None)
        page_number_int = (
            int(page_number) if page_number and page_number.isnumeric() else ''
        )

        paginator = self.get_paginator(self.get_queryset(), self.paginate_by)
        if page_number_int in paginator.page_range:
            page_obj = paginator.page(page_number)
            left_lst, right_lst = self.left_right_division(
                page_obj.object_list
            )

            profile_news_pagination = render_to_string(
                context={'page_obj': page_obj},
                template_name=self.render_pagination_template,
                request=request
            )
            profile_news = render_to_string(
                context={'left_lst': left_lst, 'right_lst': right_lst},
                template_name=self.render_news_template,
                request=request
            )

            return JsonResponse(dict(
                profile_news_pagination=profile_news_pagination,
                profile_news=profile_news
                )
            )
        return JsonResponse(
            dict(succes=False, message="Page number is not found"), status=404
        )


class NewsDetailView(DetailView):
    context_object_name = 'news_item'
    model = News
    template_name = 'pages/profile-news-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["other_news"] = News.objects.exclude(id=self.object.pk)[:5]

        return context


class HelpView(TemplateView):
    """View для страницы помощь"""
    model = Help
    template_name = 'pages/help.html'

    def get_context_data(self, **kwargs):
        context = super(HelpView, self).get_context_data(**kwargs)
        context['helps'] = Help.objects.all().order_by('-id')

        return context


class DeliveryView(TemplateView):
    model = Delivery
    template_name = 'pages/delivery-page.html'

    def get_context_data(self, **kwargs):
        context = super(DeliveryView, self).get_context_data(**kwargs)
        context['delivery'] = Delivery.objects.first()

        return context


class TypePaymentView(TemplateView):
    model = TypePayment
    template_name = 'pages/type-payment.html'

    def get_context_data(self, **kwargs):
        context = super(TypePaymentView, self).get_context_data(**kwargs)
        context['type_payment'] = TypePayment.objects.first()

        return context

from django.apps import AppConfig


class AboutUsConfig(AppConfig):
    name = 'apps.about_us'
    verbose_name = 'О нас'

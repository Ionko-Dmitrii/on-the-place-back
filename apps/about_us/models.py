from django.template.loader import render_to_string
from django.utils import timezone
from django.db import models
from django.urls import reverse
from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from phonenumber_field.modelfields import PhoneNumberField
from solo.models import SingletonModel
from ckeditor_uploader.fields import RichTextUploadingField

from apps.account.models import User
from core.utils import generate_filename


class CommonStaticPage(models.Model):  # COMM0N
    title_in_paid = models.CharField(
        verbose_name='Описание в картинке', max_length=255,
        help_text='Описание в картинке', blank=True
    )
    image_background = models.ImageField(
        verbose_name='Изображение', upload_to=generate_filename
    )
    title = models.CharField(verbose_name='Описание', max_length=255,)
    description = models.TextField(verbose_name='Текст')

    class Meta:
        abstract = True


class CommonTitle(models.Model):
    title = models.CharField(verbose_name='Описание', max_length=255,)
    description = models.TextField(verbose_name='Текст')

    class Meta:
        abstract = True


class OurAdvantages(models.Model):

    CATEGORIES = (
        (1, 'Страница о нас'),
        (2, 'Страница арендодателям'),
        (3, 'Страница стать подрядчиком '),
    )
    number = models.CharField(verbose_name='Цифра', max_length=255)
    title = models.CharField(verbose_name='Описание', max_length=255)
    description = models.TextField(verbose_name='Текст')
    category = models.SmallIntegerField(choices=CATEGORIES)

    class Meta:
        verbose_name = 'Наши преимущества'
        verbose_name_plural = 'Наши преимущества'

    def __str__(self):
        return self.title


class AboutUs(CommonStaticPage):

    logo = models.ImageField(verbose_name='Лого', upload_to=generate_filename)
    small_description = models.TextField(
        verbose_name='Текст в картинке', help_text='Текст в картинке', blank=True
    )

    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    def __str__(self):
        return self.title


class Rent(CommonStaticPage):

    class Meta:
        verbose_name = 'Арендодателям'
        verbose_name_plural = 'Арендодателям'

    def __str__(self):
        return self.title


class FormLandlord(models.Model):

    address = models.CharField(
        verbose_name='Адрес предлагаемого помещения', max_length=255
    )
    total_area = models.CharField(
        verbose_name='Общая площадь', max_length=255
    )
    rent_rate = models.CharField(
        verbose_name='Ставка арендной платы  в месяц', max_length=255
    )
    name_company = models.CharField(
        verbose_name='Название компании', max_length=255
    )
    the_contact_person = models.CharField(
        verbose_name='Контактное лицо', max_length=255
    )
    email = models.EmailField(verbose_name='e-mail')
    phone_number = models.CharField(verbose_name='Телефон', max_length=13)
    comment = models.TextField(verbose_name='Комментарий', blank=True)
    date = models.DateTimeField(verbose_name='Дата', default=timezone.now)
    viewed = models.BooleanField(verbose_name='Просмотрено', default=False)

    class Meta:
        verbose_name = 'Форма для арендодателям'
        verbose_name_plural = 'Форма для арендодателям'

    def __str__(self):
        return f'{self.name_company} - {self.email}'


class BecomeContractor(CommonStaticPage):

    title_in_paid_footer = models.CharField(
        verbose_name='Описание во второй картине', max_length=255,
        help_text='Описание во второй картине', blank=True
    )
    image_background_footer = models.ImageField(
        verbose_name='Второе изображение', upload_to=generate_filename
    )

    class Meta:
        verbose_name = 'Стать подрядчиком ON THE PLACE'
        verbose_name_plural = 'Стать подрядчиком ON THE PLACE'

    def __str__(self):
        return self.title


class PartnershipApplication(models.Model):

    name_company = models.CharField(
        verbose_name='Наименование компании', max_length=255
    )
    name = models.CharField(verbose_name='Имя', max_length=255)
    surname = models.CharField(verbose_name='Фамиля', max_length=255)
    email = models.EmailField(verbose_name='e-mail')
    comment = models.TextField(verbose_name='Комментарий', blank=True)
    date = models.DateTimeField(verbose_name='Дата', default=timezone.now)
    viewed = models.BooleanField(verbose_name='Просмотрено', default=False)

    class Meta:
        verbose_name = 'Заявка на партнерство'
        verbose_name_plural = 'Заявки на партнерство'

    def __str__(self):
        return f'{self.name_company} - {self.email}'


class ConnectPartnership(CommonStaticPage):

    title_in_page = models.CharField(
        verbose_name='Описание №2', max_length=255,
        help_text='Описание №2'
    )
    description_in_page = models.TextField(verbose_name='Текст №2')
    title_in_footer = models.CharField(
        verbose_name='Описание №3', max_length=255,
        help_text='Описание №3'
    )
    description_in_footer = models.TextField(verbose_name='Текст №3')

    class Meta:
        verbose_name = 'Подключиться к партнерской программе “Маркетплейс”'
        verbose_name_plural = 'Подключиться к партнерской программе “Маркетплейс”'

    def __str__(self):
        return self.title


class QuestionnaireForPartner(models.Model):

    name = models.CharField(verbose_name='Имя', max_length=255)
    surname = models.CharField(verbose_name='Фамилия', max_length=255)
    email = models.EmailField(verbose_name='e-mail')
    phone_number = PhoneNumberField(verbose_name='Телефон')
    name_company = models.CharField(
        verbose_name='Наименование компании', max_length=255
    )
    product_category = models.CharField(
        verbose_name='Категория товара', max_length=255
    )
    brand_name = models.CharField(
        verbose_name='Наименование бренда', max_length=255
    )
    link_to_website = models.URLField(
        verbose_name='Ссылка на сайт/социальные сети'
    )
    your_appeal = models.TextField(verbose_name='Ваше обращение', blank=True)
    date = models.DateTimeField(verbose_name='Дата', default=timezone.now)
    viewed = models.BooleanField(verbose_name='Просмотрено', default=False)

    class Meta:
        verbose_name = 'Анкета для партнеров'
        verbose_name_plural = 'Анкета для партнеров'

    def __str__(self):
        return f'{self.name_company} - {self.email}'


class News(models.Model):
    title = models.CharField(max_length=255, verbose_name="Заголовок")
    description = RichTextUploadingField(
        null=True, blank=True, verbose_name="Описание"
    )
    news_image = models.ImageField(
        null=True, blank=True, upload_to=generate_filename,
        verbose_name="Картинка для новости"
    )
    date = models.DateField(verbose_name="Дата добавления")
    special_offers = models.BooleanField(
        default=False, verbose_name="Специальное предложение"
    )
    image = models.ImageField(
        upload_to=generate_filename,
        null=True, blank=True,
        verbose_name='Изображение для слайдера на главной странице'
    )
    slug = models.SlugField(unique=True, verbose_name='Ярлык')

    class Meta:
        verbose_name = "Новости"
        verbose_name_plural = "Новости"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news-detail', kwargs={'slug': self.slug})


@receiver(post_save, sender=News)
def send(sender, created, instance, **kwargs):
    if created:
        title = instance.title
        description = instance.description
        html_message = render_to_string(
            'components/news-email.html',
            context={
                'title': title,
                'description': description
            })

        from_email = settings.EMAIL_HOST_USER
        email_user_list = User.objects.filter(
            is_subscribe=True
        ).values_list('email', flat=True)

        send_mail(
            'Новая новость',
            'Новая новость',
            from_email,
            email_user_list,
            html_message=html_message
        )


class ClientGet(CommonTitle):

    connect_partnership = models.ForeignKey(
        ConnectPartnership, on_delete=models.CASCADE,
        related_name='client_get'
    )
    image = models.ImageField(
        verbose_name='Изображение', upload_to=generate_filename
    )

    class Meta:
        verbose_name = 'Что вы получите'
        verbose_name_plural = 'Что вы получите'


class HowStart(CommonTitle):

    connect_partnership = models.ForeignKey(
        ConnectPartnership, on_delete=models.CASCADE,
        related_name='how_start'
    )

    class Meta:
        verbose_name = 'Как начать'
        verbose_name_plural = 'Как начать'


class Contact(SingletonModel):
    """Модель для контактов"""

    phone = models.CharField(verbose_name='Телефон', max_length=20)
    email = models.EmailField(verbose_name='Email', max_length=255)
    instagram = models.URLField(verbose_name='Instagram', max_length=255)
    telegram = models.URLField(verbose_name='Telegram', max_length=255)
    youtube = models.URLField(verbose_name='Youtube', max_length=255)
    facebook = models.URLField(verbose_name='Facebook', max_length=255)
    vk = models.URLField(verbose_name='Vk', max_length=255)

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return self.phone


class Help(CommonTitle):

    class Meta:
        verbose_name = "Помощь"
        verbose_name_plural = "Помощь"

    def __str__(self):
        return self.title


class Delivery(SingletonModel):

    text = RichTextUploadingField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Доставка'
        verbose_name_plural = 'Доставки'

    def __str__(self):
        return f'Доставка'


class TypePayment(SingletonModel):

    text = RichTextUploadingField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Способ оплаты'
        verbose_name_plural = 'Способы оплаты'

    def __str__(self):
        return f'Способ оплаты'

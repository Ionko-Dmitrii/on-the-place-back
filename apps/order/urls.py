from django.urls import path

from apps.cart.views import (
    ChangeCountProduct, DeleteProductFromCart, ActivatePromoCode
)
from apps.order.views import (
    ConstructorFormView, OrderReturnView,
    ReadyMadeKitsFormView, HistoryOrder, OrderView, ReturnOrderView,
    CalculateReturnProduct, ReturnOrderCreateView, TinkoffResultAPIView
)

urlpatterns = [
    path('constructor-form-form/', ConstructorFormView.as_view(),
         name='constructor-form-form'),
    path('ready-made-kit-form/', ReadyMadeKitsFormView.as_view(),
         name='ready-made-kit-form'),
    path('return/', OrderReturnView.as_view(),
         name='return'),
    path('change-count/', ChangeCountProduct.as_view(),
         name='change-count'),
    path('delete-product/', DeleteProductFromCart.as_view(),
         name='delete-product'),
    path('activate-promo-code/', ActivatePromoCode.as_view(),
         name='activate-promo-code'),
    path('order/', OrderView.as_view(), name='order'),
    path('order-history/', HistoryOrder.as_view(), name='order-history'),
    path('return-order/<int:pk>/', ReturnOrderView.as_view(), name='return-order'),
    path('create-return-order/', ReturnOrderCreateView.as_view(),
         name='create-return-order'),
    path('calculate-return-product/', CalculateReturnProduct.as_view(),
         name='calculate-return-product'),
    path('tinkoff-result-api/', TinkoffResultAPIView.as_view(), name='tinkoff-result-api'),
]

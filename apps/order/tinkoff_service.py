import hashlib
import json

import requests
from django.conf import settings
from django.urls import reverse


class TinkoffRedirectService:
    """Параметры для оплаты тинькофф"""

    def __init__(self, order):
        self.order = order

    def send_request(self, request):
        resp = requests.post(
            url=settings.PAYMENT_URL,
            headers={'Content-Type': 'application/json'},
            data=json.dumps(self.generate_tinkoff_url(request))
        )

        return resp

    def generate_tinkoff_url(self, request):

        products = [
            {
                "Name": product.title,
                "Price": round(
                    float(
                        product.total_price - (
                                product.total_price / 100 *
                                (product.discount +
                                 self.order['price']['promo_code_discount'])
                        )
                    ) * product.quantity, 0) * 100,
                "Quantity": product.quantity,
                "Amount": round(
                    float(
                        product.total_price -
                        (product.total_price / 100 *
                         (product.discount +
                          self.order['price']['promo_code_discount']))
                    ) * product.quantity, 0) * 100,
                "PaymentMethod": "full_prepayment",
                "PaymentObject": "commodity",
                "Tax": "none",
            } for product in self.order['order_products_list']
        ]

        products.append(
            {
                "Name": 'delivery',
                "Price": self.order['order'].delivery_price * 100,
                "Quantity": 1,
                "Amount": self.order['order'].delivery_price * 100,
                "PaymentMethod": "full_prepayment",
                "PaymentObject": "commodity",
                "Tax": "none",
            },
        )

        param = {
            "TerminalKey": settings.TERMINAL_KEY,
            "Amount": int(self.order['price']['price_with_discount']) * 100,
            "OrderId": self.order['order'].id,
            "Token": self.get_token(),
            "SuccessURL": request.build_absolute_uri('tinkoff-success'),
            "FailURL": request.build_absolute_uri(reverse('tinkoff-error')),
            "NotificationURL": request.build_absolute_uri(
                reverse('tinkoff-result-api')),
            "Receipt": {
                "Email": self.order['order'].email,
                "Taxation": "usn_income",
                "Items": products
            }
        }

        return param

    def get_token(self):
        token = {
            "Amount": int(self.order['price']['price_with_discount']) * 100,
            "OrderId": self.order['order'].id,
            "Password": settings.TERMINAL_PASSWORD,
            "TerminalKey": settings.TERMINAL_KEY,
        }
        token_str = "".join('{}'.format(k) for k in token.values())
        token_hash = hashlib.sha256(token_str.encode('utf-8')).hexdigest()

        return token_hash

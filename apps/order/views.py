import json
from datetime import timedelta

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView, TemplateView, ListView, DetailView
from django.views.generic.base import View
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from apps.account.admin import User
from apps.cart.models import CartItem, Cart
from apps.cart.service import Basket, OrderService
from apps.common.models import (
    Product, ProductColor, ProductColorImage, ProductSize
)
from apps.order.forms import (
    ConstructorFormApplicationForm, OrderForm, ReturnOrderForm
)
from apps.order.models import (
    OrderProductConstructor, Order, ReasonReturn, ReturnOrder, OrderItem
)
from apps.common.service import save_order_application, send_message_to_email
from apps.order.tinkoff_service import TinkoffRedirectService


class ConstructorFormView(CreateView):
    """View для зафвки на коструктор форм"""

    template_name = 'pages/category-for-teams-generator.html'
    form_class = ConstructorFormApplicationForm
    render_template_name = 'components/email.html'

    def form_valid(self, form):
        product_list = list()
        product_color_list = list()
        product_price = 0
        order_form, string_order_info = save_order_application(form)
        order_form.save()

        for product in string_order_info:
            product_obj = Product.objects.get(id=product)
            product_color = ProductColor.objects.get(
                id=string_order_info[product])
            product_price += product_obj.price
            obj = OrderProductConstructor(
                order=order_form,
                product=product_obj,
                title=product_obj.title,
                price=product_obj.price,
                article=product_color.article,
                color=product_color.color,
                color_id=string_order_info[product],
            )
            product_list.append(obj)
            product_color_list.append([obj, product_color.color])

        order_form.price = product_price * form.cleaned_data.get(
            'product_count')
        order_form.save()
        OrderProductConstructor.objects.bulk_create(product_list)

        send_message_to_email(
            order_form, product_color_list, self.render_template_name
        )

        return JsonResponse(dict(success=True, message='success'), status=200)

    def form_invalid(self, form):
        messages = []
        for i in form.errors:
            messages.append([i, form.errors[i]])
        return JsonResponse(
            dict(success=False, message=messages), status=400)


class OrderReturnView(TemplateView):
    """View для страницы возврата заказа"""

    template_name = 'pages/returning.html'


class ReadyMadeKitsFormView(CreateView):
    """View для заявки на готовые комплекты"""

    template_name = 'pages/ready-made-kits-detail.html'
    form_class = ConstructorFormApplicationForm
    render_template_name = 'components/email.html'

    def form_valid(self, form):
        kit_order_form, string_order_info = save_order_application(form)
        product_obj = Product.objects.get(pk=string_order_info[0])
        product_color = ProductColor.objects.get(pk=string_order_info[1])
        kit_order_form.price = product_obj.get_discount_sum
        kit_order_form.save()

        obj = OrderProductConstructor(
            order=kit_order_form,
            product=product_obj,
            title=product_obj.title,
            price=product_obj.get_discount_sum,
            article=product_color.article,
            color=product_color.color,
            color_id=product_color.id
        )

        obj.save()

        send_message_to_email(
            kit_order_form, [[obj, product_color.color]],
            self.render_template_name
        )

        return JsonResponse(dict(success=True, message='success'), status=200)

    def form_invalid(self, form):
        messages = []
        for error in form.errors:
            messages.append([error, form.errors[error]])
        return JsonResponse(
            dict(success=False, message=messages), status=400)


class OrderView(CreateView):
    """View для создания заказа"""

    template_name = 'pages/cart.html'
    form_class = OrderForm
    render_template_name = 'components/order-message.html'

    def form_valid(self, form):

        if self.request.user.is_authenticated:
            product_id_list = json.loads(
                form.cleaned_data.get('product_id')
            )['product']

            if form.cleaned_data.get('subscribe'):
                user = self.request.user
                user.is_subscribe = True
                user.save(update_fields=['is_subscribe'])

            product_list = (
                [get_object_or_404(CartItem, id=product[0]),
                 get_object_or_404(Product, id=product[1]),
                 ProductColorImage.objects.filter(
                     product_color__id=product[2]
                 ).first()] for product in product_id_list
            )

            order_class = OrderService.save_order_to_database(
                self.request, form, product_list
            )

            if form.cleaned_data.get('type_payment') == 'online_payment':
                tinkoff_class = TinkoffRedirectService(
                    order=order_class
                )
                resp = tinkoff_class.send_request(request=self.request)

                if resp.json()['Success'] is True:
                    return JsonResponse(
                        dict(
                            success=True, online_payment=True,
                            message='Необходимо перейти по ссылке ниже для оплаты заказа',
                            response=resp.json()
                        ), status=200
                    )
                return JsonResponse(
                    dict(success=False,
                         message=[['type_payment', ['Ошибка оплаты, попробуйте снова!']]]),
                    status=400
                )

            get_object_or_404(Cart, user=self.request.user).delete()
            Basket.promo_code = 0
            Basket.promo_code_code = None

            send_message_to_email(
                order_class['order'], order_class['order_products_list'],
                self.render_template_name
            )

            return JsonResponse(dict(
                success=True, online_payment=False,
                message='Заказ успешно оформлен!'
            ), status=200)
        else:
            cookie = Basket.get_products_id_from_cookie(self.request)
            Basket.cookie = cookie
            product_list = [
                [
                    get_object_or_404(Product, id=product[0]),
                    get_object_or_404(ProductColor, id=product[1]),
                    get_object_or_404(ProductSize, id=product[2]),
                    ProductColorImage.objects.filter(
                        product_color__id=product[1]
                    ).first(),
                    product[3]
                ] for product in cookie['products_list']
            ]
            order_class = OrderService.save_order_to_database(
                self.request, form, product_list
            )

            if form.cleaned_data.get('type_payment') == 'online_payment':

                tinkoff_class = TinkoffRedirectService(
                    order=order_class
                )
                resp = tinkoff_class.send_request(request=self.request)

                if resp.json()['Success'] is True:
                    cookie['cookie_list'] = []

                    response = JsonResponse(
                        dict(
                            success=True, online_payment=True,
                            message='Необходимо перейти по ссылке ниже для оплаты заказа',
                            response=resp.json()
                        ), status=200
                    )

                    Basket.set_cookie(response, settings.BASKET_COOKIE_NAME,
                                      json.dumps(cookie['cookie_list']))

                    return response

                return JsonResponse(
                    dict(success=False,
                         message=[['type_payment', ['Ошибка оплаты, попробуйте снова!']]]),
                    status=400
                )

            Basket.promo_code = 0
            Basket.promo_code_code = None
            send_message_to_email(
                order_class['order'], order_class['order_products_list'],
                self.render_template_name
            )
            cookie['cookie_list'] = []
            response = JsonResponse(dict(
                success=True, message='Заказ успешно оформлен!',
                online_payment=False,
            ), status=200)
            Basket.set_cookie(response, settings.BASKET_COOKIE_NAME,
                              json.dumps(cookie['cookie_list']))

            return response

    def form_invalid(self, form):

        messages = []
        for error in form.errors:
            messages.append([error, form.errors[error]])
        return JsonResponse(
            dict(success=False, message=messages), status=400)


class TinkoffResultAPIView(APIView):
    render_template_name = 'components/order-message.html'
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        order_id = request.data['OrderId']
        order = get_object_or_404(Order, id=order_id)
        user = User.objects.filter(order_user__id=order_id).first()
        order_products = OrderItem.objects.filter(order=order)
        if request.data['Status'] == 'CONFIRMED':
            Basket.promo_code = 0
            Basket.promo_code_code = None
            order.status_payment = True
            order.save(update_fields=['status_payment'])
            send_message_to_email(
                order, order_products,
                self.render_template_name
            )

            if user:
                get_object_or_404(Cart, user=user).delete()

        elif request.data['Status'] == 'REJECTED':
            if user:
                order.delete()


class HistoryOrder(ListView):
    template_name = 'pages/profile-history.html'
    context_object_name = 'order_history'

    def get_queryset(self):
        queryset = Order.objects.filter(
            user=self.request.user
        ).prefetch_related('order_products')

        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(HistoryOrder, self).get_context_data(**kwargs)
        context['return_order'] = ReturnOrder.objects.filter(
            status='return', user=self.request.user
        )

        return context


class ReturnOrderView(DetailView):
    """View для страницы возврата заказа"""
    model = Order
    template_name = 'pages/returning.html'
    context_object_name = 'order'

    def get_context_data(self, **kwargs):
        context = super(ReturnOrderView, self).get_context_data(**kwargs)
        context['reasons_return'] = ReasonReturn.objects.all()

        return context


class ReturnOrderCreateView(CreateView):
    """View для страницы возврата заказа"""

    form_class = ReturnOrderForm
    template_name = 'pages/returning.html'
    render_template_name = 'components/order-message.html'

    def form_valid(self, form):
        user = self.request.user
        today_date = timezone.now()
        product_list_id = form.cleaned_data.get('product_list')
        order_id = form.cleaned_data.get('order_id')
        order = get_object_or_404(Order, id=order_id)
        order_products_list = OrderItem.objects.filter(id__in=product_list_id)
        if (today_date - order.create_at) < timedelta(days=14):
            OrderService.save_return_order_to_database(
                order, order_products_list, order_id, product_list_id, form,
                user, self.render_template_name
            )

            return JsonResponse(
                dict(
                    success=True,
                    message='Возврат заказа успешно завершен!, С вами свяжется наш менеджер!',
                    url=self.request.build_absolute_uri(reverse('order-history'))
                ), status=200)
        else:
            return JsonResponse(
                dict(
                    success=False,
                    message=[
                        ['date-error', 'Время возврата истекло! (14 дней)']
                    ]), status=400)

    def form_invalid(self, form):
        messages = []
        for error in form.errors:
            messages.append([error, form.errors[error]])

        return JsonResponse(dict(success=False, message=messages), status=400)


class CalculateReturnProduct(View):
    """View для подсчета суммы стоимости возврата товара"""

    def post(self, request, *args, **kwargs):
        product_id_list = request.POST.get('product_id').split(',')
        order_id = request.POST.get('order_id')

        calc = OrderService.calc_return_order(product_id_list, order_id)

        return JsonResponse(dict(success=True, calc=calc, status=200))

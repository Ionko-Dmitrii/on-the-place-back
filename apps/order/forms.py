import json

from django import forms
from django.core.exceptions import ValidationError

from apps.order.models import ConstructorFormApplication, Order, ReturnOrder


class ConstructorFormApplicationForm(forms.ModelForm):
    """Форма для заявок на конструктор форм"""
    order_info = forms.CharField()
    product_count = forms.IntegerField()

    class Meta:
        model = ConstructorFormApplication
        fields = (
            'name', 'phone', 'email', 'text', 'order_info', 'product_count'
        )


class OrderForm(forms.ModelForm):
    """Форма для заказов"""

    product_id = forms.CharField()
    subscribe = forms.BooleanField(required=False)
    time_delivery = forms.CharField()
    promo_code_title = forms.CharField(required=False)

    class Meta:
        model = Order
        fields = (
            'name', 'last_name', 'second_name', 'address', 'email', 'date',
            'text', 'type_payment', 'type_delivery', 'product_id', 'subscribe',
            'address_delivery', 'time_delivery', 'promo_code_title'
        )

    def clean_product_id(self):
        data = self.cleaned_data.get('product_id')

        if len(json.loads(data).get('product')) < 1:
            raise ValidationError("Добавьте товары в корзину!")

        return data

    def clean(self):
        cleaned_data = super().clean()
        date = cleaned_data.get('date')
        type_delivery = cleaned_data.get('type_delivery')

        if type_delivery == 'express_delivery':
            if date is None:
                self.add_error('date', 'Выберите дату!')


class ReturnOrderForm(forms.ModelForm):
    """Форма для возврата заказа"""

    product_list = forms.CharField(required=False)
    order_id = forms.CharField()

    class Meta:
        model = ReturnOrder
        fields = ('text', 'reason_return', 'product_list', 'order_id')

    def clean_product_list(self):
        data = self.cleaned_data.get('product_list').split(',')
        if not data[0]:
            raise ValidationError("Выберите товары для возврата!")

        return data

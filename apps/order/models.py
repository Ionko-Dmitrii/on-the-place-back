from django.db import models
from django.utils import timezone

from apps.account.admin import User
from apps.cart.models import Cart, CartItem
from apps.common.models import Product, PromoCode
from core.constants import (
    ORDER_STATUSES, TYPE_PAYMENT, TYPE_DELIVERY, RETURN_ORDER_STATUSES
)
from core.utils import generate_filename


class ConstructorFormApplication(models.Model):
    """Модель для заявок на коструктор формы"""

    name = models.CharField(verbose_name='Имя', max_length=255)
    phone = models.CharField(verbose_name='Телефон', max_length=50)
    email = models.EmailField(verbose_name='Email', max_length=50)
    text = models.TextField(verbose_name='Коментарии', null=True, blank=True)
    count = models.IntegerField(
        verbose_name='Колличество комплектов', null=True, blank=True
    )
    price = models.IntegerField(verbose_name='Итого', null=True, blank=True)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания заказа', null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Заявка на коструктор формы'
        verbose_name_plural = 'Заявки на костркутор форм'

    def __str__(self):
        return self.name


class OrderProductConstructor(models.Model):
    """Модель продуктов для заявок на коструктор формы"""

    order = models.ForeignKey(
        verbose_name='Заказ', to=ConstructorFormApplication,
        on_delete=models.CASCADE, related_name='order_products'
    )
    product = models.ForeignKey(
        verbose_name='Товар', to=Product, on_delete=models.SET_NULL, null=True,
        related_name='order_products'
    )
    title = models.CharField(verbose_name='Название товара', max_length=255)
    price = models.DecimalField(
        verbose_name='Цена товара', decimal_places=2, max_digits=8
    )
    article = models.CharField(verbose_name='Артикул', max_length=255)
    color = models.CharField(verbose_name='Цвет', max_length=255)
    color_id = models.IntegerField(
        verbose_name='Id цвета', blank=True, null=True
    )

    class Meta:
        verbose_name = 'Товар заказа'
        verbose_name_plural = 'Товары заказа'

    def __str__(self):
        return self.title


class AbstractOrder(models.Model):
    """Модель заказа"""

    status_payment = models.BooleanField(
        verbose_name='Статус оплаты', default=False,
    )
    name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    second_name = models.CharField(verbose_name='Отчество', max_length=255)
    address = models.CharField(verbose_name='Адрес доставки', max_length=255)
    address_delivery = models.CharField(
        verbose_name='Пункт выдачи заказа', max_length=255, null=True, blank=True
    )
    email = models.EmailField(verbose_name='Email', max_length=50)
    time_from = models.TimeField(
        verbose_name='Время доставки с:', null=True, blank=True
    )
    time_to = models.TimeField(
        verbose_name='Время доставки до:', null=True, blank=True
    )
    date = models.DateField(
        verbose_name='Дата доставки', null=True, blank=True
    )
    delivery_price = models.DecimalField(
        verbose_name='Стоимость доставки', decimal_places=0, max_digits=8
    )
    create_at = models.DateTimeField(
        verbose_name='Дата и время заказа', default=timezone.now
    )
    id = models.AutoField(
        verbose_name='Номер заказа', auto_created=True, primary_key=True,
        serialize=False,
        )
    type_delivery = models.CharField(
        choices=TYPE_DELIVERY, max_length=150, default='pickup',
        verbose_name='Способ доставки'
    )
    type_payment = models.CharField(
        choices=TYPE_PAYMENT, max_length=150, default='payment_on_delivery',
        verbose_name='Способ оплаты'
    )
    total_price = models.DecimalField(
        verbose_name='Обшая сумма без скидки', decimal_places=0, max_digits=8
    )
    price_with_discount = models.DecimalField(
        verbose_name='Общая сумма со скидкой', decimal_places=0, max_digits=8
    )
    price_discount = models.DecimalField(
        verbose_name='Сумма скидки', decimal_places=0, max_digits=8
    )
    promo_code_discount = models.DecimalField(
        verbose_name='Скидка промокода',  decimal_places=0, max_digits=8
    )
    promo_code_discount_price = models.DecimalField(
        verbose_name='Сумма скидки промокода',  decimal_places=0, max_digits=8
    )
    quantity = models.PositiveIntegerField(
        default=0, verbose_name='Колличество товаров'
    )
    text = models.TextField(verbose_name='Коментарии', null=True, blank=True)

    class Meta:
        abstract = True


class AbstractOrderItem(models.Model):
    """Модель товара для заказа"""

    article = models.CharField(verbose_name='Артикул', max_length=255)
    title = models.CharField(verbose_name='Название товара', max_length=255)
    color = models.CharField(verbose_name='Цвет товара', max_length=255)
    size = models.CharField(verbose_name='Размер товара', max_length=255)
    quantity = models.PositiveIntegerField(
        default=0, verbose_name='Колличество товаров'
    )
    total_price = models.DecimalField(
        verbose_name='Сумма без скидки', decimal_places=0, max_digits=8
    )
    price_with_discount = models.DecimalField(
        verbose_name='Сумма со скидкой',  decimal_places=0, max_digits=8
    )
    discount = models.DecimalField(
        verbose_name='Скидка', decimal_places=0, max_digits=8
    )
    price_discount = models.DecimalField(
        verbose_name='Сумма скидки', decimal_places=0, max_digits=8
    )
    image = models.ImageField(
        verbose_name='Изображение', null=True, upload_to=generate_filename,
        blank=True,
    )
    text = models.TextField(verbose_name='Описание')

    class Meta:
        abstract = True


class Order(AbstractOrder):
    """Модель заказа"""

    user = models.ForeignKey(
        to=User, verbose_name='Пользователь', related_name='order_user',
        on_delete=models.SET_NULL, blank=True, null=True
    )
    status = models.CharField(
        choices=ORDER_STATUSES, max_length=150, default='progress',
        verbose_name='Статус заказа'
    )

    class Meta:
        verbose_name = 'Зазаз'
        verbose_name_plural = 'Заказы'
        ordering = ('-create_at',)

    def __str__(self):
        return self.name


class OrderItem(AbstractOrderItem):
    """Модель товара для заказа"""

    order = models.ForeignKey(
        verbose_name='Заказ', to=Order,
        on_delete=models.CASCADE, related_name='order_products'
    )
    product = models.ForeignKey(
        to=Product, verbose_name='Товар', on_delete=models.SET_NULL, null=True,
        related_name='order_product_list', blank=True
    )

    class Meta:
        verbose_name = 'Товар заказа'
        verbose_name_plural = 'Товары заказов'

    def __str__(self):
        return self.title

    @property
    def get_all_price(self):
        price_all = self.quantity * self.price_with_discount

        return price_all


class ReasonReturn(models.Model):
    """Модель для причины возврата заказа"""

    text = models.TextField(verbose_name='Причина возврата')

    class Meta:
        verbose_name = 'Причина возврата заказа'
        verbose_name_plural = 'Причины возврата заказа'

    def __str__(self):
        return self.text


class ReturnOrder(AbstractOrder):
    """Модель возврата заказа"""

    user = models.ForeignKey(
        to=User, verbose_name='Пользователь', related_name='return_order_user',
        on_delete=models.SET_NULL, blank=True, null=True
    )
    reason_return = models.CharField(
        verbose_name='Причина возврата', max_length=255
    )
    status = models.CharField(
        choices=RETURN_ORDER_STATUSES, max_length=150, default='return',
        verbose_name='Статус заказа'
    )

    class Meta:
        verbose_name = 'Возврат заказа'
        verbose_name_plural = 'Возврат заказов'
        ordering = ('-create_at',)

    def __str__(self):
        return self.name


class ReturnOrderItem(AbstractOrderItem):
    """Модель возврата заказа"""

    return_order = models.ForeignKey(
        verbose_name='Заказ', to=ReturnOrder,
        on_delete=models.CASCADE, related_name='return_order_products'
    )

    class Meta:
        verbose_name = 'Товар возвращенного заказа'
        verbose_name_plural = 'Товары возвращенных заказов'

    def __str__(self):
        return self.title

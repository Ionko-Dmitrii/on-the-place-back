from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from django.utils.safestring import mark_safe

from apps.order.models import (
    ConstructorFormApplication, OrderProductConstructor, Order, OrderItem,
    ReasonReturn, ReturnOrder, ReturnOrderItem
)


class OrderProductConstructorAdmin(admin.TabularInline):
    model = OrderProductConstructor
    extra = 0
    fields = ('get_img_display', 'title', 'price', 'article', 'color')
    readonly_fields = ('get_img_display', 'title', 'price', 'article', 'color')
    can_delete = False

    def get_img_display(self, obj):
        if obj.product:
            try:
                image_url = obj.product.product_colors.get(
                    id=obj.color_id
                ).product_color_images.first().image.url
            except ObjectDoesNotExist:
                image_url = '/static/img/png/150x150.png'
        else:
            image_url = '/static/img/png/150x150.png'
        return mark_safe(
            f'<img src={image_url} style="width:100px; height:100px;">'
        )

    get_img_display.short_description = 'Изображение'


@admin.register(ConstructorFormApplication)
class ConstructorFormApplicationAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'created_at')
    inlines = [OrderProductConstructorAdmin]
    readonly_fields = (
        'name', 'phone', 'email', 'price', 'count', 'text', 'created_at'
    )


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 0
    fields = (
        'article', 'title', 'color', 'size', 'total_price',
        'price_with_discount', 'price_discount', 'discount',
        'get_image_display', 'quantity', 'text'
              )
    readonly_fields = (
        'get_image_display', 'quantity', 'price_discount', 'discount', 'color',
        'total_price', 'price_with_discount', 'article', 'size', 'text',
        'title'
    )

    def get_image_display(self, obj):
        if obj.image:
            try:
                image_url = obj.image.url
            except ObjectDoesNotExist:
                image_url = '/static/img/png/150x150.png'
        else:
            image_url = '/static/img/png/150x150.png'

        return mark_safe(
            f'<img src={image_url} style="width:100px; height:100px;">'
        )

    get_image_display.short_description = 'Изображение'


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemInline]
    list_display = (
        'name', 'last_name', 'create_at', 'email', 'status', 'status_payment'
    )
    list_editable = ('status', )
    fieldsets = (
        (None, {
            'fields': (
                'create_at', 'user', 'name', 'last_name', 'second_name', 'email',
                'address', 'text'
            )
        }),
        ('Информация о заказе', {
            'fields': (
                'id', 'status_payment', 'status', 'type_payment',
                'address_delivery',
                'type_delivery', 'time_from', 'time_to', 'delivery_price',
                'price_discount', 'promo_code_discount',
                'promo_code_discount_price',
                'total_price', 'price_with_discount', 'quantity'
            )
        }),
    )
    readonly_fields = (
        'create_at', 'name', 'last_name', 'second_name', 'email', 'address',
        'text', 'delivery_price', 'total_price', 'price_with_discount',
        'price_discount', 'promo_code_discount', 'promo_code_discount_price',
        'quantity', 'type_delivery', 'time_from', 'time_to', 'address_delivery',
        'type_payment', 'id', 'user', 'status_payment'
    )


class ReturnOrderItemInline(admin.StackedInline):
    extra = 0
    model = ReturnOrderItem
    fields = (
        'article', 'title', 'color', 'size', 'total_price',
        'price_with_discount', 'price_discount', 'discount',
        'get_image_display', 'quantity', 'text'
              )
    readonly_fields = (
        'get_image_display', 'quantity', 'price_discount', 'discount', 'color',
        'total_price', 'price_with_discount', 'article', 'size', 'text', 'title'
    )

    def get_image_display(self, obj):
        if obj.image:
            try:
                image_url = obj.image.url
            except ObjectDoesNotExist:
                image_url = '/static/img/png/150x150.png'
        else:
            image_url = '/static/img/png/150x150.png'

        return mark_safe(
            f'<img src={image_url} style="width:100px; height:100px;">'
        )

    get_image_display.short_description = 'Изображение'


@admin.register(ReturnOrder)
class ReturnOrderAdmin(admin.ModelAdmin):
    inlines = [ReturnOrderItemInline]
    list_display = ('name', 'last_name', 'create_at', 'email', 'status')
    list_editable = ('status', )
    fieldsets = (
        (None, {
            'fields': (
                'create_at', 'user', 'name', 'last_name', 'second_name', 'email',
                'address', 'text', 'reason_return'
            )
        }),
        ('Информация о заказе', {
            'fields': (
                'id', 'status_payment', 'status', 'type_payment',
                'address_delivery',
                'type_delivery', 'time_from', 'time_to', 'delivery_price',
                'price_discount', 'promo_code_discount',
                'promo_code_discount_price',
                'total_price', 'price_with_discount', 'quantity'
            )
        }),
    )
    readonly_fields = (
        'create_at', 'name', 'last_name', 'second_name', 'email', 'address',
        'text', 'delivery_price', 'total_price', 'price_with_discount',
        'price_discount', 'promo_code_discount', 'promo_code_discount_price',
        'quantity', 'type_delivery', 'time_from', 'time_to', 'address_delivery',
        'type_payment', 'id', 'user', 'reason_return', 'status', 'status_payment'
    )


@admin.register(ReasonReturn)
class ReasonReturnAdmin(admin.ModelAdmin):
    pass

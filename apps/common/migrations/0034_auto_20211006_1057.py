# Generated by Django 2.2 on 2021-10-06 04:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0033_auto_20211005_1851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='Отображать'),
        ),
    ]

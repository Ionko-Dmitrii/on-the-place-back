from django.db import models
from django.urls import reverse
from django.utils import timezone

from apps.account.models import User
from core.constants import (
    MAIN_CATEGORY_TYPE, CATEGORY_TYPE, KIND_OF_SPORTS, SALE,
    CONSTRUCTOR_TYPE, READY_MADE_KIT, BANNER_PAGES
)
from core.utils import generate_filename


class MainCategory(models.Model):
    """Модель для главных категорий"""

    type_main_category = models.CharField(
        verbose_name="Главные категории", choices=MAIN_CATEGORY_TYPE,
        default=SALE, max_length=255, unique=True
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)

    class Meta:
        verbose_name = 'Главная категория'
        verbose_name_plural = 'Главные категории'

    def __str__(self):
        return self.get_type_main_category_display()

    def get_absolute_url(self):
        return reverse('category', kwargs={'slug': self.slug})


class Category(models.Model):
    """Модель для категорий"""

    type_category = models.CharField(
        verbose_name="Категории", choices=CATEGORY_TYPE, max_length=255,
        default=KIND_OF_SPORTS
    )
    main_category = models.ForeignKey(
        MainCategory, verbose_name="Главная категория",
        on_delete=models.CASCADE,
        related_name='categories'
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return (
                self.main_category.get_type_main_category_display()
                + " -> " + self.get_type_category_display()
        )


class Subcategory(models.Model):
    """Модель для подкатегорий"""

    title = models.CharField(
        verbose_name="Подкатегории", max_length=255
    )
    category = models.ForeignKey(
        Category, verbose_name="Категория", on_delete=models.CASCADE,
        related_name='subcategories'
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'

    def __str__(self):
        return (
                self.category.main_category.get_type_main_category_display()
                + " -> " + self.category.get_type_category_display()
                + " -> " + self.title
        )

    def get_absolute_url(self):
        return reverse('subcategory', kwargs={'slug': self.slug})


class Material(models.Model):
    """Модель для материала"""

    title = models.CharField(
        verbose_name="Материал", max_length=255
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)

    class Meta:
        verbose_name = 'Материал'
        verbose_name_plural = 'Материалы'

    def __str__(self):
        return self.title


class Brand(models.Model):
    """Модель для бренда"""

    title = models.CharField(
        verbose_name="Бренд", max_length=255
    )
    icon = models.ImageField(
        verbose_name='Изображение', upload_to=generate_filename
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)
    is_active = models.BooleanField(
        verbose_name='Показать бренд как активный', default=False
    )

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.title


class Tag(models.Model):
    """Модель для тегов"""

    title = models.CharField(verbose_name='Название тега', max_length=255)

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.title


class Product(models.Model):

    title = models.CharField(verbose_name='Название товара', max_length=255)
    description = models.TextField(verbose_name='Описание товара')
    price = models.DecimalField(
        verbose_name="Цена", decimal_places=0, max_digits=8
    )
    discount = models.DecimalField(
        verbose_name="Скидка в %", decimal_places=0, max_digits=3, default=0
    )
    is_new = models.BooleanField(verbose_name='Новинка', default=False)
    is_active = models.BooleanField(verbose_name='Отображать', default=False)
    is_populate = models.BooleanField(verbose_name='Популярное', default=False)
    type_constructor = models.CharField(
        verbose_name="Тип конструктора", choices=CONSTRUCTOR_TYPE, null=True,
        max_length=255, blank=True
    )
    image = models.ImageField(
        verbose_name='Изображение для слайдера на главной странице', null=True,
        blank=True, upload_to=generate_filename,
    )
    show_on_main = models.BooleanField(
        verbose_name="Отображать в слайдере", default=False
    )
    subcategory = models.ManyToManyField(
        Subcategory, verbose_name='Подкатегории', related_name='subcategories'
    )
    material = models.ManyToManyField(
        Material, verbose_name='Материал', related_name='materials'
    )
    brand = models.ManyToManyField(
        Brand, verbose_name='Бранд', related_name='brands', blank=True,
    )
    tag = models.ForeignKey(
        Tag, verbose_name='Теги', related_name='products',
        on_delete=models.CASCADE, null=True, blank=True
    )
    slug = models.SlugField(verbose_name='Ярлык', unique=True)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['-id']

    def __str__(self):
        return self.title

    @property
    def get_discount_sum(self):
        return round(self.price - (self.price / 100 * self.discount))

    @property
    def price_discount(self):
        return self.price / 100 * self.discount

    def get_absolute_url(self):
        if self.type_constructor == READY_MADE_KIT:
            return reverse('ready-made-kits-detail', kwargs={'slug': self.slug})
        return reverse('product', kwargs={'slug': self.slug})


class DescriptionConstructor(models.Model):
    """Модель описания для готовых комплектов"""

    title = models.CharField(verbose_name='Название', max_length=255)
    article = models.CharField(verbose_name='Артикул', max_length=255)
    model = models.CharField(verbose_name='Модель', max_length=255)
    description = models.TextField(verbose_name='Описание')
    product = models.ForeignKey(
        Product, verbose_name='Товар', on_delete=models.CASCADE,
        related_name='description_ready_made_kit'
    )

    class Meta:
        verbose_name = 'Описание для готовых комлектов'
        verbose_name_plural = 'Описания для готовых комлектов'

    def __str__(self):
        return self.title


class Characteristic(models.Model):
    """Модель для характеристик товара"""

    title = models.CharField(
        verbose_name='Название характеристики', max_length=255
    )
    name = models.CharField(
        verbose_name='Описание характеристики', max_length=255
    )
    product = models.ForeignKey(
        Product, verbose_name='Товар', on_delete=models.CASCADE,
        related_name='characteristics'
    )

    class Meta:
        verbose_name = 'Характеристика'
        verbose_name_plural = 'Характеристики'

    def __str__(self):
        return self.title


class Color(models.Model):
    """Модель для цвета"""

    hex = models.CharField(verbose_name='Кодировка цвета', max_length=255)
    name = models.CharField(verbose_name='Название цвета', max_length=255)
    border = models.BooleanField(
        verbose_name='Показать бордер', default=False,
        help_text='Использовать для белого цвета'
    )

    class Meta:
        verbose_name = 'Цвет'
        verbose_name_plural = 'Цвета'

    def __str__(self):
        return self.name


class ProductColor(models.Model):
    """Модель цвета для товара"""

    color = models.ForeignKey(
        Color, verbose_name='Цвета', on_delete=models.CASCADE,
        related_name='colors'
    )
    product = models.ForeignKey(
        Product, verbose_name='Цвета товаров', on_delete=models.CASCADE,
        related_name='product_colors'
    )
    article = models.CharField(
        verbose_name='Артикул', max_length=255, unique=True
    )

    class Meta:
        verbose_name = 'Цвет товара'
        verbose_name_plural = 'Цвета товаров'

    def __str__(self):
        return self.color.name


class ProductColorImage(models.Model):
    """Модель изображения для каждого цвета товара"""

    image = models.ImageField(
        verbose_name='Изображения', upload_to=generate_filename
    )
    product_color = models.ForeignKey(
        ProductColor, verbose_name='Изображение для товара',
        on_delete=models.CASCADE, related_name='product_color_images'
    )

    class Meta:
        verbose_name = 'Изображнние'
        verbose_name_plural = 'Изображения'

    def __str__(self):
        return f'Изображение № {self.id}'


class Size(models.Model):
    """Модель размеров"""

    title = models.CharField(
        verbose_name='Название размера', max_length=255,
        help_text='Размер для обуви'
    )
    eu_size = models.CharField(
        verbose_name='Размер - EUR', max_length=255, blank=True, null=True
    )
    us_size = models.CharField(
        verbose_name='Размер - UK', max_length=255, blank=True, null=True
    )
    jp_size = models.CharField(
        verbose_name='Размер - JP', max_length=255, blank=True, null=True
    )
    ru_size = models.CharField(
        verbose_name='Размер - RU', max_length=255, blank=True, null=True
    )

    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'

    def __str__(self):
        return f'{self.title}: {self.eu_size}'


class ProductSize(models.Model):
    """Модель размеров для товара"""

    is_available = models.BooleanField(
        verbose_name="Есть в наличии", default=True
    )
    quantity = models.PositiveIntegerField(
        verbose_name="Колличество товара одного размера", default=0
    )
    size = models.ForeignKey(
        Size, verbose_name='Размер', on_delete=models.CASCADE,
        related_name='sizes'
    )

    product_color = models.ForeignKey(
        ProductColor, verbose_name='Размер товара', on_delete=models.CASCADE,
        related_name='product_color', null=True, blank=True
    )

    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'

    def __str__(self):
        return f"{self.size.title} -> {self.size.eu_size}"


class Comment(models.Model):
    """Модель комментариев для товара"""

    rating = models.PositiveIntegerField(
        verbose_name='Рейтинг звезд', default=0
    )
    description = models.TextField(verbose_name='Текст комментария')
    product = models.ForeignKey(
        Product, verbose_name='Комментарий для товара', on_delete=models.CASCADE,
        related_name='product_comments'
    )
    user = models.ForeignKey(
        User, verbose_name='Автор комментария', on_delete=models.CASCADE,
        related_name='user_comments'
    )
    likes = models.ManyToManyField(
        User, related_name='liked_comments', verbose_name='Лайки', blank=True
    )
    dislikes = models.ManyToManyField(
        User, related_name='disliked_comments', verbose_name='Дизлайки',
        blank=True
    )
    is_active = models.BooleanField(verbose_name='Отображать', default=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return f'{self.user.first_name}: {self.rating}'


class ImageForCommandCategory(models.Model):
    """"Модель изображений для категорий команд"""

    image_one = models.ImageField(
        verbose_name='Изобрадение №1', upload_to=generate_filename
    )

    image_two = models.ImageField(
        verbose_name='Изобрадение №2', upload_to=generate_filename
    )

    image_three = models.ImageField(
        verbose_name='Изобрадение №3', upload_to=generate_filename
    )

    class Meta:
        verbose_name = 'Изображения категорий команд'
        verbose_name_plural = 'Изображения категорий команд'

    def __str__(self):
        return f'Изображение № {self.id}'


class ImageForBanner(models.Model):

    desktop_image = models.ImageField(
        verbose_name="Баннер для десктопной версии", null=True,
        blank=True, upload_to=generate_filename
    )
    mobile_image = models.ImageField(
        verbose_name="Баннер для мобильной версии", null=True,
        blank=True, upload_to=generate_filename
    )
    page_name = models.CharField(
        verbose_name="Страница для вывода", choices=BANNER_PAGES,
        max_length=255
    )

    class Meta:
        verbose_name = "Картинка для баннера"
        verbose_name_plural = "Картинки для баннера"

    def __str__(self):
        return self.page_name


class PromoCode(models.Model):

    title = models.CharField(verbose_name='Название промокода', max_length=255)
    code = models.CharField(verbose_name='Промокод', max_length=255)
    discount = models.DecimalField(
        verbose_name="Скидка в процентах(%)", decimal_places=0, max_digits=2
    )
    quantity = models.PositiveIntegerField(
        verbose_name="Колличество использований промокода", default=0
    )
    created_at = models.DateTimeField(
        verbose_name='Дата начала действия промокода', default=timezone.now
    )
    time_end = models.DateTimeField(verbose_name='Дата окончания промокода')

    class Meta:
        verbose_name = "Промокод"
        verbose_name_plural = "Промокоды"

    def __str__(self):
        return self.title


class TableSize(models.Model):

    title = models.CharField(verbose_name='Название таблицы', max_length=255)
    brand = models.ManyToManyField(
        Brand, verbose_name='Бранд', related_name='brand_tables_sizes',
        blank=True
    )
    subcategory = models.ManyToManyField(
        Subcategory, verbose_name='Категория', related_name='subcategory_tables_sizes',
        blank=True
    )

    class Meta:
        verbose_name = 'Таблица размеров'
        verbose_name_plural = 'Таблицы размеров'

    def __str__(self):
        return self.title


class RowTableSize(models.Model):

    size_name = models.CharField(
        verbose_name='Наименование размеров', max_length=255
    )
    table = models.ForeignKey(
        TableSize, verbose_name='table', on_delete=models.CASCADE,
        related_name='sizes_name'
    )

    class Meta:
        verbose_name = 'Наименование размеров'
        verbose_name_plural = 'Наименование размеров'

    def __str__(self):
        return self.size_name


class ColumnTableSize(models.Model):

    size = models.CharField(verbose_name='Размер', max_length=255)
    row_table_size = models.ForeignKey(
        RowTableSize, verbose_name='Наименование размеров',
        on_delete=models.CASCADE, related_name='sizes'
    )

    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'

    def __str__(self):
        return self.size

from django.utils.safestring import mark_safe
from django import template

register = template.Library()


@register.filter()
def highlight_search_input(text, value):
    value = value.title()

    highlighted = text.replace(value, f'<span>{value}</span>')
    return mark_safe(highlighted)

import json

from django.conf import settings
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.db.models import Q, F, Value
from django.shortcuts import redirect
from django.template import loader
from django.views.generic import ListView

from apps.common.models import (
    Product, MainCategory, Category, Material, Tag, Color, Size, Subcategory,
    Comment
)
from core.constants import MAN, CHILDREN, WOMAN, SALE, KIND_OF_SPORTS

SEARCH_RANK = 0.3


class FilterAndSort:
    """Класс для фильтрации и сортировки товаров"""

    @staticmethod
    def get_filter_products(
            category, fav_categories, constructor_products, brand, tag
    ):

        query = Q()

        if category.get('color_filter'):
            query.add(
                Q(product_colors__color_id__in=category.get('color_filter')),
                query.connector
            )
        if category.get('material_filter'):
            query.add(
                Q(material__id__in=category.get('material_filter')),
                query.connector
            )

        if category.get('size_filter'):
            query.add(
                Q(product_colors__product_color__size_id__in=category.get(
                    'size_filter'),
                    product_colors__product_color__is_available=True),
                query.connector
            )

        if category.get('brand_filter'):
            query.add(
                Q(brand__title__icontains=category.get('brand_filter')[0]),
                query.connector
            )

        if category.get('sport_filter'):
            query.add(
                Q(subcategory__title__in=category.get('sport_filter')),
                query.connector
            )

        if category.get('tag_filter'):
            query.add(
                Q(tag__id__in=(category.get('tag_filter'))),
                query.connector
            )

        if fav_categories:
            query.add(Q(subcategory__in=fav_categories), query.connector)

        if constructor_products:
            query.add(
                Q(type_constructor__in=constructor_products), query.connector
            )

        if brand:
            query.add(Q(brand=brand), query.connector)

        if tag:
            query.add(Q(tag=tag), query.connector)

        query.add(Q(is_active=True), query.connector)

        sort = FilterAndSort.get_name_sorting(category)

        if category.get('price_from_filter') or category.get('price_to_filter') or category.get('sort_filter'):
            if category.get('price_from_filter') and (
                    len(category.get('price_from_filter')[0])) > 0:
                gte = category.get('price_from_filter')[0]
            else:
                gte = 1

            if category.get('price_to_filter') and (
                    len(category.get('price_to_filter')[0])) > 0:
                lte = category.get('price_to_filter')[0]
            else:
                lte = 800000

            products = (
                Product.objects.annotate(
                    sale_price=F('price') - (
                            F('price') / Value(100) * F('discount')
                    )
                ).filter(query, sale_price__range=(gte, lte))
                .prefetch_related('product_colors')
                .prefetch_related('product_colors__product_color_images')
                .prefetch_related('favorite_products__product')
                .order_by(sort).distinct()
                )
        else:
            products = (
                Product.objects.filter(query).prefetch_related('product_colors')
                .prefetch_related('product_colors__product_color_images')
                .prefetch_related('favorite_products__product')
                .order_by(sort).distinct()
                )

        return products

    @classmethod
    def get_name_sorting(cls, category):

        sort_item = ""
        order_by_sort = '-id'

        if category.get('sort_filter'):
            sort_item = category.get('sort_filter')[0]

        if sort_item == 'populate':
            order_by_sort = '-is_populate'
        if sort_item == 'price':
            order_by_sort = 'sale_price'
        if sort_item == 'new':
            order_by_sort = '-is_new'
        if sort_item == 'discount':
            order_by_sort = '-discount'
        if sort_item == 'recommended':
            order_by_sort = 'subcategory__favorite_sports__subcategory__title'

        return order_by_sort


def get_category():
    context = {
        'man_category': MainCategory.objects.filter(type_main_category=MAN)
            .prefetch_related('categories',
                              'categories__subcategories').first(),
        'woman_category': MainCategory.objects.filter(type_main_category=WOMAN)
            .prefetch_related('categories',
                              'categories__subcategories').first(),
        'children_category': MainCategory.objects.filter(
            type_main_category=CHILDREN
        ).prefetch_related('categories', 'categories__subcategories').first(),
        'sale_category': MainCategory.objects.filter(type_main_category=SALE)
            .prefetch_related('categories',
                              'categories__subcategories').first(),
        'news_category': Category.objects.filter(
            subcategories__subcategories__is_new=True
        ).prefetch_related('subcategories').distinct(),
    }

    return context


class AbstracFiltertListView(ListView):
    """AbstractView для фильтра"""
    products_filter = list()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AbstracFiltertListView, self).get_context_data(**kwargs)
        context['material_list'] = Material.objects.filter(
            materials__in=self.products_filter
        ).distinct()
        context['tag_list'] = Tag.objects.filter(
            products__in=self.products_filter
        ).distinct()
        context['color_list'] = Color.objects.filter(
            colors__product__in=self.products_filter
        ).distinct()
        context['size_list'] = Size.objects.filter(
            sizes__product_color__product__in=self.products_filter
        ).distinct()
        context['type_sport_list'] = (Subcategory.objects.filter(
                category__type_category=KIND_OF_SPORTS,
                subcategories__in=self.products_filter
        ).distinct('title'))

        return context


def save_order_application(order_form):
    pre_saved_order_form = order_form.save(commit=False)
    pre_saved_order_form.name = order_form.cleaned_data.get('name')
    pre_saved_order_form.phone = order_form.cleaned_data.get('phone')
    pre_saved_order_form.email = order_form.cleaned_data.get('email')
    pre_saved_order_form.text = order_form.cleaned_data.get('text')
    pre_saved_order_form.count = order_form.cleaned_data.get('product_count')
    string_order_info = json.loads(order_form.cleaned_data.get('order_info'))
    return pre_saved_order_form, string_order_info


def send_message_to_email(order, products, template_name):
    html_message = loader.render_to_string(
        template_name,
        {'products': products, 'form': order}
    )
    send_mail(
        'Заказ on-the-place',
        'Заказ on-the-place',
        settings.EMAIL_HOST_USER,
        [settings.EMAIL_HOST_USER],
        html_message=html_message,
        fail_silently=False
    )


class CommentService:

    @staticmethod
    def post_comment(request, form, **kwargs):
        """ Send a comment on the product """
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.product_id = Product.objects.get(slug=kwargs['slug']).id
            comment.save()
            return redirect('product', kwargs['slug'])

    @staticmethod
    def get_comment_list(product, paginate_by):
        """ Comment """
        comment_paginator = Paginator(
            Comment.objects.filter(product=product, is_active=True)
            .order_by('-id'), paginate_by)

        paginator = comment_paginator.page
        try:
            comments_list = paginator(1)
        except PageNotAnInteger:
            comments_list = paginator(1)
        except EmptyPage:
            comments_list = paginator(comment_paginator.num_pages)
        return comments_list


class AutoCompleteService:

    @staticmethod
    def get_autocomplete_search_vector(model, title, subcategory: str):
        query = SearchQuery(title, subcategory, config='russian')
        vector = SearchVector(
            'title_vector', 'subcategory_vector', weight='A', config='russian'
        )
        rank = SearchRank(vector, query)

        if model.__name__ == 'Product':
            queryset = (
                model.objects
                    .select_related('product')
                    .filter(is_active=True)
            )
        else:
            queryset = model.objects.filter(is_active=True)

        return (
            queryset
            .annotate(rank=rank)
            .filter(rank__gte=SEARCH_RANK)
            .order_by('-rank')
        )


class ProductSearchService(AutoCompleteService):
    model = Product.objects.filter(is_active=True)

    @classmethod
    def get_autocomplete_search_vector(cls, title, subcategory: str):
        return super().get_autocomplete_search_vector(
            cls.model, title=title, subcategory=subcategory
        )

    @classmethod
    def search_by_title(cls, value: str):
        return (
            cls.model.filter(
                Q(title__istartswith=value) |
                Q(subcategory__title__istartswith=value) |
                Q(product_colors__article__startswith=value)
            ).distinct()
        )

import json
from itertools import chain

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q, F, Value
from django.http import JsonResponse
from django.template import loader
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from django.views import View

from apps.account.models import Favorite
from apps.common.forms import CommentForm
from apps.common.models import (
    ImageForCommandCategory, Brand,
    Subcategory, Product, MainCategory, ProductColorImage,
    Comment, ProductColor, ProductSize, ImageForBanner, Tag, TableSize
)
from apps.about_us.models import News

from apps.common.service import (
    AbstracFiltertListView, CommentService, ProductSearchService
)

from apps.common.service import FilterAndSort
from core.constants import (
    KIND_OF_SPORTS, READY_MADE_KIT, CONSTRUCTOR_ONE, CONSTRUCTOR_TWO,
    CONSTRUCTOR_THREE, READY_MADE_KIT_PAGE, SPECIAL_OFFER_PAGE,
    COMMAND_CATEGORY_PAGE
)


class IndexView(TemplateView):
    """View для главной страницы"""

    template_name = 'pages/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['brand_list'] = Brand.objects.all()
        products = Product.objects.filter(show_on_main=True)
        news = News.objects.filter(special_offers=True)
        resulted_list = list(chain(products, news))
        context['resulted_list'] = resulted_list
        context['tags'] = Tag.objects.all()
        return context


class TagView(AbstracFiltertListView):
    model = Tag
    template_name = 'pages/tag-filters.html'
    render_template_name = 'components/render-products.html'
    context_object_name = 'tags'

    def get_context_data(self, **kwargs):
        tag = self.kwargs['pk']
        self.products_filter = Product.objects.filter(
            tag__id=tag).prefetch_related(
            'product_colors', 'product_colors__product_color_images',
            'favorite_products__product'
        )
        context = super(TagView, self).get_context_data(**kwargs)
        context['products'] = self.products_filter

        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        brand = None
        fav_categories = None
        tag = Tag.objects.get(id=self.kwargs['pk'])

        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class SpecialOffersView(LoginRequiredMixin, AbstracFiltertListView):
    """View для страницы спец предложений"""

    template_name = 'pages/special-offers.html'
    render_template_name = 'components/render-products.html'
    context_object_name = 'products'

    def get_queryset(self):
        categories = self.request.user.sports_for_user.subcategory.all()
        queryset = (
            Product.objects.filter(
                subcategory__in=categories,
                is_active=True
            ).distinct()
                .prefetch_related(
                'product_colors', 'product_colors__product_color_images',
                'favorite_products__product')
        )
        self.products_filter = queryset

        return queryset

    def get_context_data(self, **kwargs):
        context = super(SpecialOffersView, self).get_context_data(**kwargs)
        context["banners"] = (
            ImageForBanner.objects.filter(
                page_name__exact=SPECIAL_OFFER_PAGE).last()
        )

        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        brand = None
        tag = None
        fav_categories = self.request.user.sports_for_user.subcategory.all()
        category = json.loads(request.POST.get('categoryId'))

        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class AddRemoveFavoriteProduct(View):
    """View для добавления в избранное"""
    render_template_name = 'components/render-favorite-product.html'

    def post(self, request, *args, **kwargs):

        product_id = request.POST.get('product_id')

        favorite_obj, _ = Favorite.objects.get_or_create(user=self.request.user)
        product = Product.objects.get(id=product_id)

        if product in favorite_obj.product.all():
            favorite_obj.product.remove(product)
            added_or_remove = 0
        else:
            favorite_obj.product.add(product)
            added_or_remove = 1

        product_html = loader.render_to_string(
            self.render_template_name, {
                'products': product,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(
            product=product_html,
            added_or_remove=added_or_remove,
        ))


class FavoriteView(LoginRequiredMixin, TemplateView):
    """View для страницы избранное"""

    template_name = 'pages/favorite.html'
    render_template_name = 'components/render-products.html'

    def get_context_data(self, **kwargs):
        context = super(FavoriteView, self).get_context_data(**kwargs)
        favorite_products = self.request.user.favorite_user.product.all()
        context['products'] = (
            Product.objects.filter(
                favorite_products__product__in=favorite_products, is_active=True
            ).prefetch_related(
                'product_colors', 'product_colors__product_color_images',
                'favorite_products__product'
            )
        )

        return context

    def post(self, request, *args, **kwargs):
        favorite_products = self.request.user.favorite_user.product.all()
        category = json.loads(request.POST.get('categoryId'))
        sort = FilterAndSort.get_name_sorting(category)

        products = Product.objects.annotate(
            sale_price=F('price') - (F('price') / Value(100) * F('discount'))
        ).filter(
            favorite_products__product__in=favorite_products, is_active=True
        ).distinct()

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class CategoryView(AbstracFiltertListView):
    """View страниц для главных категорий"""

    template_name = 'pages/category_page.html'
    context_object_name = 'category_products'
    render_template_name = 'components/render-products.html'

    def get_queryset(self):
        main_category = MainCategory.objects.get(slug=self.kwargs.get('slug'))

        return main_category

    def get_context_data(self, *, object_list=None, **kwargs):
        self.products_filter = (
            Product.objects.filter(
                subcategory__category__main_category__slug=self.kwargs.get(
                    'slug'), is_active=True
            ).prefetch_related(
                'product_colors', 'product_colors__product_color_images',
                'favorite_products__product'
            ).distinct()
        )
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['products'] = self.products_filter
        context['subcategories_list'] = Subcategory.objects.all(
        ).distinct('title').exclude(category__type_category=KIND_OF_SPORTS)
        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        brand = None
        tag = None
        fav_categories = Subcategory.objects.filter(
            category__main_category__slug=self.kwargs.get('slug'),
            subcategories__is_active=True
        )
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class SubcategoryView(AbstracFiltertListView, ListView):
    """View для страниц подкатегорий"""

    template_name = 'pages/subcategory_page.html'
    render_template_name = 'components/render-products.html'
    context_object_name = 'subcategory'

    def get_queryset(self):
        subcategory = Subcategory.objects.get(slug=self.kwargs.get('slug'))

        return subcategory

    def get_context_data(self, **kwargs):
        subcategory = Subcategory.objects.get(slug=self.kwargs.get('slug'))
        self.products_filter = (
            Product.objects.filter(subcategory=subcategory, is_active=True)
                .distinct().prefetch_related(
                'product_colors', 'product_colors__product_color_images',
                'favorite_products__product'
            )
        )
        context = super(SubcategoryView, self).get_context_data(**kwargs)
        context['products'] = self.products_filter
        context['subcategories_list'] = Subcategory.objects.all(
        ).distinct('title').exclude(category__type_category=KIND_OF_SPORTS)

        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        brand = None
        tag = None
        fav_categories = Subcategory.objects.filter(
            slug=self.kwargs.get('slug'), subcategories__is_active=True
        )
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user

            }
        )

        return JsonResponse(dict(products_html=products_html))


class BrandView(AbstracFiltertListView):
    """View для страницы брендов"""

    template_name = 'pages/brand.html'
    context_object_name = 'brand'
    render_template_name = 'components/render-products.html'

    def get_queryset(self):
        queryset = Brand.objects.get(slug=self.kwargs.get('slug'))
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        self.products_filter = (
            Product.objects.filter(
                brand__slug=self.kwargs.get('slug'),
                is_active=True
            ).prefetch_related(
                'product_colors', 'product_colors__product_color_images',
                'favorite_products__product'
            ).distinct()
        )
        context = super(BrandView, self).get_context_data(**kwargs)
        context['products'] = self.products_filter
        context['subcategories_list'] = Subcategory.objects.filter(
            subcategories__in=self.products_filter
        ).exclude(category__type_category=KIND_OF_SPORTS).distinct('title')

        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        fav_categories = None
        tag = None
        brand = Brand.objects.get(slug=self.kwargs.get('slug'))
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class CategoryTeamView(AbstracFiltertListView):
    """View для страницы команд"""

    template_name = 'pages/category-for-teams.html'
    render_template_name = 'components/render-products.html'
    context_object_name = 'category_command_products'
    type_constructor = (
        CONSTRUCTOR_ONE, CONSTRUCTOR_TWO, CONSTRUCTOR_THREE, READY_MADE_KIT
    )
    queryset = Product.objects.filter(
        type_constructor__in=type_constructor, is_active=True
    ).prefetch_related(
        'product_colors', 'product_colors__product_color_images',
        'favorite_products__product'
    ).distinct()
    products_filter = queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CategoryTeamView, self).get_context_data(**kwargs)
        context['subcategories_list'] = Subcategory.objects.all(
        ).distinct('title').exclude(category__type_category=KIND_OF_SPORTS)
        context["banners"] = (
            ImageForBanner.objects.filter(
                page_name__exact=COMMAND_CATEGORY_PAGE).last()
        )

        return context

    def post(self, request, *args, **kwargs):
        fav_categories = None
        brand = None
        tag = None
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, self.type_constructor, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user

            }
        )

        return JsonResponse(dict(products_html=products_html))


class CommandListView(TemplateView):
    """View для страницы списка команд"""

    template_name = 'pages/commands.html'

    def get_context_data(self, **kwargs):
        context = super(CommandListView, self).get_context_data(**kwargs)
        context['command_categories_images'] = (
            ImageForCommandCategory.objects.first()
        )

        return context


class ReadyMadeKitsView(ListView):
    """View для страницы готовые комплекты"""

    template_name = 'pages/ready-made-kits.html'
    render_template_name = 'components/render-products.html'
    context_object_name = 'constructor_products'
    ready_made_kit = (READY_MADE_KIT,)
    queryset = Product.objects.filter(
        type_constructor=READY_MADE_KIT, is_active=True
    )
    products_filter = queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ReadyMadeKitsView, self).get_context_data(**kwargs)
        context['type_sport_list'] = Subcategory.objects.filter(
            category__type_category=KIND_OF_SPORTS,
            subcategories__type_constructor__in=self.ready_made_kit
        ).distinct('title')
        context["banners"] = (
            ImageForBanner.objects.filter(
                page_name__exact=READY_MADE_KIT_PAGE).last()
        )

        return context

    def post(self, request, *args, **kwargs):
        fav_categories = None
        brand = None
        tag = None
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, self.ready_made_kit, brand, tag
        )

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))


class CommentLikeView(View):

    def post(self, request, *args, **kwargs):
        user = request.user
        pk = self.kwargs['pk']
        comment = Comment.objects.get(pk=pk)
        if user.is_authenticated and request.POST.get('like') == '1':
            if comment.likes.filter(id=self.request.user.id).exists():
                comment.likes.remove(user)
                result_like = 0

                return JsonResponse(dict(
                    like_count=comment.likes.count(),
                    result_like=result_like,
                ))
            else:
                if comment.dislikes.filter(id=self.request.user.id).exists():
                    comment.dislikes.remove(user)
                    comment.likes.add(user)
                    result_dislike = 0
                    result_like = 1

                    return JsonResponse(dict(
                        like_count=comment.likes.count(),
                        dislikes_count=comment.dislikes.count(),
                        result_like=result_like,
                        result_dislike=result_dislike
                    ))
                else:
                    comment.likes.add(user)
                    result_like = 1

                    return JsonResponse(dict(
                        like_count=comment.likes.count(),
                        result_like=result_like
                    ))


class CommentDislikeView(View):

    def post(self, request, *args, **kwargs):
        user = request.user
        pk = self.kwargs['pk']
        comment = Comment.objects.get(pk=pk)
        if user.is_authenticated and request.POST.get('dislike') == '0':
            if comment.dislikes.filter(id=self.request.user.id).exists():
                comment.dislikes.remove(user)
                result_dislike = 0

                return JsonResponse(dict(
                    dislikes_count=comment.dislikes.count(),
                    result_dislike=result_dislike,
                ))

            else:
                if comment.likes.filter(id=self.request.user.id).exists():
                    comment.likes.remove(user)
                    comment.dislikes.add(user)
                    result_dislike = 1
                    result_like = 0

                    return JsonResponse(dict(
                        like_count=comment.likes.count(),
                        dislikes_count=comment.dislikes.count(),
                        result_like=result_like,
                        result_dislike=result_dislike
                    ))
                else:
                    comment.dislikes.add(user)
                    result_dislike = 1

                    return JsonResponse(dict(
                        dislikes_count=comment.dislikes.count(),
                        result_dislike=result_dislike,
                    ))


class ProductDetailView(DetailView):
    """View для страницы товара"""
    queryset = Product.objects.all()
    template_name = 'pages/product.html'
    model = Product
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        product = Product.objects.get(slug=self.kwargs['slug'])
        context['products'] = Product.objects.all()
        color_first = ProductColor.objects.filter(product=product).first()
        context['product_sizes'] = ProductSize.objects.filter(
            product_color=color_first
        )
        context['similar_products'] = Product.objects.order_by('title').exclude(
            pk=product.pk
        )[:8]
        context['product_comments'] = CommentService.get_comment_list(
            product, self.paginate_by
        )

        context['table_size'] = TableSize.objects.filter(
            subcategory__in=product.subcategory.all(),
            brand__in=product.brand.all()
        ).prefetch_related('sizes_name', 'sizes_name__sizes').first()

        return context


class AddCommentView(CreateView):
    form_class = CommentForm
    render_template_new_comment = 'components/render-new-comment.html'    

    def post(self, request, *args, **kwargs):
        user = self.request.user
        product_id = request.POST.get('productId')
        form = self.form_class(request.POST)
        if user.is_authenticated and form.is_valid:
            comment = form.save(commit=False)
            comment.user = user
            comment.product_id=product_id
            comment.save()

            comment_html = loader.render_to_string(self.render_template_new_comment, {
                'comment': comment
            })

            return JsonResponse(dict(
                comment_html=comment_html,
                success=True,
                message="Отзыв добавлен",
            ), status=200)


class ProductImageForColorView(TemplateView):
    render_template_name = "components/render-image.html"
    render_template_name2 = "components/render-size.html"
    render_template_mobile_size = "components/mobile-render-size.html"

    def post(self, request, *args, **kwargs):
        color_id = request.POST.get('colorId')

        sizes = ProductSize.objects.filter(product_color__id=color_id)
        images = ProductColorImage.objects.filter(
            product_color__id=color_id
        )

        article = ProductColor.objects.get(id=color_id).article

        sizes_html = loader.render_to_string(self.render_template_name2, {
            'sizes': sizes
        })

        mobile_sizes_html = loader.render_to_string(
            self.render_template_mobile_size, {
                'sizes': sizes
            }
        )

        images_html = loader.render_to_string(self.render_template_name, {
            'images': images,
        })

        return JsonResponse(dict(
            images_html=images_html,
            sizes_html=sizes_html,
            mobile_sizes_html=mobile_sizes_html,
            article=article
        ))


class CommentView(ListView):
    render_template_name = "components/render-comments.html"
    render_template_more_reviews = "components/render_more_reviews.html"
    paginate_by = 5

    def post(self, request, *args, **kwargs):
        page = request.POST.get('page')
        product_id = request.POST.get('productId')

        comment_paginator = Paginator(
            Comment.objects.filter(product__id=product_id, is_active=True)
                .order_by('-id'), self.paginate_by)

        try:
            comments_list = comment_paginator.page(page)
        except PageNotAnInteger:
            comments_list = comment_paginator.page(1)
        except EmptyPage:
            comments_list = comment_paginator.page(comment_paginator.num_pages)

        comments_html = loader.render_to_string(
            self.render_template_name,
            {
                'product_comments': comments_list,
                'user': self.request.user
             }
        )

        more_reviews_html = loader.render_to_string(
            self.render_template_more_reviews,
            {
                'page_obj': comments_list,
                'product': product_id
            }
        )


        return JsonResponse(dict(
            comments_html=comments_html,
            more_reviews_html=more_reviews_html,
        ))


class OrderHistoryView(TemplateView):
    """View для страницы история заказов"""

    template_name = 'pages/profile-history.html'


class ConstructorView(AbstracFiltertListView):
    """View для страницы конструктор форм"""

    template_name = 'pages/category-for-teams-generator.html'
    render_template_name = 'components/render-constructor-products.html'
    render_template_name_info = 'components/constructor-form-info.html'
    context_object_name = 'products'
    type_constructor = (CONSTRUCTOR_ONE, CONSTRUCTOR_TWO, CONSTRUCTOR_THREE)
    queryset = (
        Product.objects.filter(
            type_constructor__in=type_constructor, is_active=True
        )
        .prefetch_related('product_colors')
        .prefetch_related('product_colors__product_color_images')
        .prefetch_related('favorite_products__product')
    )
    products_filter = queryset

    def get_context_data(self, **kwargs):
        context = super(ConstructorView, self).get_context_data(**kwargs)
        context['subcategories_list'] = Subcategory.objects.all(
        ).distinct('title').exclude(category__type_category=KIND_OF_SPORTS)

        return context

    def post(self, request, *args, **kwargs):
        fav_categories = None
        brand = None
        tag = None
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, self.type_constructor, brand, tag
        )
        if category.get('color_filter'):
            colors = ProductColor.objects.filter(
                product__type_constructor__in=self.type_constructor,
                color__id__in=category.get('color_filter')
            )
        else:
            colors = ProductColor.objects.all()

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'colors': colors
            }
        )

        products_html_info = loader.render_to_string(
            self.render_template_name_info,
            {
                'products': products,
            }
        )

        return JsonResponse(dict(
            products_html=products_html, products_html_info=products_html_info)
        )


class NewsProductCategory(AbstracFiltertListView):
    """View страницы новинки"""

    template_name = 'pages/news-category_page.html'
    context_object_name = 'news_category_products'
    render_template_name = 'components/render-products.html'

    def get_queryset(self):
        main_category = Product.objects.filter(is_new=True)
        self.products_filter = main_category

        return main_category

    def get_context_data(self, **kwargs):
        context = super(NewsProductCategory, self).get_context_data(**kwargs)
        context['subcategories_list'] = Subcategory.objects.filter(
            subcategories__is_new=True
        ).distinct('title').exclude(category__type_category=KIND_OF_SPORTS)

        return context

    def post(self, request, *args, **kwargs):
        constructor_products = None
        brand = None
        tag = None
        fav_categories = Subcategory.objects.filter(
            subcategories__is_new=True
        )
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, constructor_products, brand, tag
        )

        products = products.filter(is_new=True)

        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user

            }
        )

        return JsonResponse(dict(products_html=products_html))


class ReadyMadeKitsDetailView(DetailView):
    """View для детальной страницы готовых комплектов"""

    render_color_template = 'components/kits-images-color.html'
    template_name = 'pages/ready-made-kits-detail.html'
    context_object_name = 'product'
    queryset = Product.objects.filter(
        type_constructor=READY_MADE_KIT, is_active=True
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = self.get_object()
        context['kit_images'] = (
            product.product_colors.first().product_color_images.all()
        )
        return context

    def post(self, request, *args, **kwargs):
        color_id = request.POST.get('color_id')
        product = self.get_object()
        kit_images = (
            product.product_colors.get(pk=color_id).product_color_images.all()
        )
        kit_images_html = loader.render_to_string(
            self.render_color_template,
            {'kit_images': kit_images}
        )
        return JsonResponse(dict(kit_images_html=kit_images_html))


class SearchView(TemplateView):
    render_template = 'components/ajax-search-result.html'

    def post(self, request):
        value = request.POST.get('value')

        if not value:
            return JsonResponse(dict(success=False), status=404)

        product = ProductSearchService.search_by_title(value)

        if product:
            html_response = loader.render_to_string(
                self.render_template,
                {'render_product': product, 'value': value}
            )

            return JsonResponse(dict(
                success=True, message='Найдены совпадения',
                result_search=html_response
            ), status=200)

        return JsonResponse(dict(
            success=False, message='Не найдены совпадения'
        ), status=404)


class SearchResultView(AbstracFiltertListView):
    template_name = 'pages/products_search_result.html'
    context_object_name = 'search'
    render_template_name = 'components/render-products.html'

    def get_queryset(self):
        value = self.request.GET.get('value')
        model = Product.objects.filter(is_active=True)
        product = model.filter(
            Q(title__istartswith=value) |
            Q(subcategory__title__istartswith=value) |
            Q(product_colors__article__startswith=value)
        ).distinct()
        self.products_filter = product

        return product

    def get_context_data(self, **kwargs):
        context = super(SearchResultView, self).get_context_data(**kwargs)
        context['subcategories_list'] = (
            Subcategory.objects.filter(subcategories__in=self.products_filter)
                .distinct('title')
                .exclude(category__type_category=KIND_OF_SPORTS)
        )
        value = self.request.GET.get('value')
        context['product'] = ProductSearchService.search_by_title(value)

        return context

    def post(self, request, *args, **kwargs):
        value = self.request.GET.get('value')
        fav_categories = None
        brand = None
        constructor_products = None
        tag = None
        category = json.loads(request.POST.get('categoryId'))
        products = FilterAndSort.get_filter_products(
            category, fav_categories, brand, constructor_products, tag
        )
        products = products.filter(
            Q(title__istartswith=value) |
            Q(subcategory__title__istartswith=value) |
            Q(product_colors__article__startswith=value)
        ).distinct()
        products_html = loader.render_to_string(
            self.render_template_name,
            {
                'products': products,
                'user': self.request.user
            }
        )

        return JsonResponse(dict(products_html=products_html))

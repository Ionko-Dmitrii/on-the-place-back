from django.urls import path

from apps.common.views import (
    NewsProductCategory, SpecialOffersView, CategoryTeamView, FavoriteView,
    SubcategoryView, ConstructorView, ReadyMadeKitsDetailView, BrandView,
    ReadyMadeKitsView, AddRemoveFavoriteProduct, CommandListView, CategoryView,
    ProductDetailView, OrderHistoryView, CommentLikeView,
    ProductImageForColorView, CommentView, SearchView, SearchResultView, TagView,
    AddCommentView, CommentDislikeView
)


urlpatterns = [
    path('special-offers/', SpecialOffersView.as_view(), name='special-offers'),
    path('category-team/', CategoryTeamView.as_view(), name='category-team'),
    path('favorite/', FavoriteView.as_view(), name='favorite'),
    path('ready-made-kits/', ReadyMadeKitsView.as_view(),
         name='ready-made-kits'),
    path('add-favorite/', AddRemoveFavoriteProduct.as_view(),
         name='add-favorite'),
    path('command-list/', CommandListView.as_view(), name='command-list'),
    path('product/<slug:slug>/', ProductDetailView.as_view(), name='product'),
    path('add-comment/', AddCommentView.as_view(), name='add-comment'),

    path('comment/', CommentView.as_view(), name='comments'),
    path('product-color/', ProductImageForColorView.as_view(),
         name='product_color_images'),
    path('<int:pk>/like/', CommentLikeView.as_view(), name='like'),
    path('<int:pk>/dislike/', CommentDislikeView.as_view(), name='dislike'),

    path('order-history/', OrderHistoryView.as_view(), name='order-history'),
    path('constructor-form/', ConstructorView.as_view(), name='constructor-form'),
    path('ready-made-kits-detail/<slug:slug>/', ReadyMadeKitsDetailView.as_view(),
         name='ready-made-kits-detail'),
    path('subcategory/<slug:slug>/', SubcategoryView.as_view(),
         name='subcategory'),
    path('news-product-category/', NewsProductCategory.as_view(),
         name='news-product-category'),
    path('brand/<slug:slug>/', BrandView.as_view(), name='brand'),
    path('search/', SearchView.as_view(), name='search'),
    path('search-result/', SearchResultView.as_view(), name='search-result'),
    path('<slug:slug>/', CategoryView.as_view(), name='category'),
    path('tags/<int:pk>/', TagView.as_view(), name='tags'),
]

from django import forms
from django.forms import TextInput
from apps.common.models import Color, Comment


class ColorAdminForm(forms.ModelForm):
    class Meta:
        model = Color
        fields = '__all__'
        widgets = {
            'hex': TextInput(attrs={'type': 'color'})
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('rating', 'description')

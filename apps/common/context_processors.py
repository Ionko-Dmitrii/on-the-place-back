from apps.about_us.models import Contact
from apps.cart.models import CartItem
from apps.cart.service import Basket
from apps.common.service import get_category


def get_my_context(request):
    categories = get_category()
    delivery_price = 0

    if request.user.is_authenticated:
        cart_products = CartItem.objects.filter(
            cart__user=request.user
        ).order_by('-id')
    else:
        cookie = Basket.get_products_id_from_cookie(request)
        Basket.cookie = cookie
        cart_products = Basket.get_products_from_cookie_for_render(cookie)
    context = {
        'man_category': categories['man_category'],
        'woman_category': categories['woman_category'],
        'children_category': categories['children_category'],
        'sale_category': categories['sale_category'],
        'news_category': categories['news_category'],
        'contacts': Contact.objects.first(),
        'get_products_cart': cart_products,
        'price': (
            Basket.get_price_product_list(request, delivery_price)
        )
    }

    return context

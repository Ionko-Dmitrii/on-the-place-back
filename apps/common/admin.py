from django.contrib import admin
from django.utils.safestring import mark_safe

from apps.common.forms import ColorAdminForm
from apps.common.models import (
    MainCategory, Category, Subcategory, Product, Material, Brand,
    Characteristic, Color, ProductColor, ProductColorImage, ProductSize, Size,
    Comment, DescriptionConstructor, Tag, ImageForCommandCategory,
    ImageForBanner, PromoCode, TableSize, RowTableSize, ColumnTableSize
)

import nested_admin


@admin.register(MainCategory)
class MainCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('type_main_category',)}


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active')
    prepopulated_fields = {'slug': ('title',), }
    list_editable = ('is_active', )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('type_category', 'main_category'), }


@admin.register(Subcategory)
class SubcategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', 'category'), }


@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',), }


class CharacteristicInline(nested_admin.NestedTabularInline):
    model = Characteristic
    extra = 0


class ProductColorImageInline(nested_admin.NestedTabularInline):
    model = ProductColorImage
    extra = 0


class ProductSizeInline(nested_admin.NestedTabularInline):
    model = ProductSize
    extra = 0


class ProductColorInline(nested_admin.NestedTabularInline):
    model = ProductColor
    inlines = [ProductColorImageInline, ProductSizeInline]
    extra = 0


class CommentInline(nested_admin.NestedTabularInline):
    model = Comment
    extra = 0
    readonly_fields = (
        'rating', 'description', 'product', 'user', 'likes', 'dislikes'
    )


class DescriptionConstructorInline(nested_admin.NestedTabularInline):
    model = DescriptionConstructor
    extra = 0


@admin.register(Product)
class ProductAdmin(nested_admin.NestedModelAdmin):
    filter_horizontal = ('subcategory', 'material', 'brand')
    inlines = (CharacteristicInline, ProductColorInline,
               DescriptionConstructorInline, CommentInline)
    prepopulated_fields = {'slug': ('title',), }
    list_display = ('title', 'is_new', 'is_active',
                    'is_populate', 'show_on_main')
    list_editable = ('is_new', 'is_active', 'is_populate',
                     'show_on_main')


@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    form = ColorAdminForm
    list_display = ('name', 'get_hex_color',)
    fieldsets = (
        (None, {
            'fields': ('hex', 'name', 'border')
        }),
    )

    def get_hex_color(self, obj):
        return mark_safe(
            f'<span style="background-color:{obj.hex}; width:12px; height:12px;'
            f' display:block; border: 1px solid gray"></span>'
        )

    get_hex_color.short_description = 'Цвет'


@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    list_display = ('title', 'eu_size', 'us_size', 'jp_size', 'ru_size')


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(PromoCode)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at', 'time_end', 'get_discount', 'quantity')

    def get_discount(self, obj):
        return f'{obj.discount} %'

    get_discount.short_description = 'Скидка'


@admin.register(ImageForCommandCategory)
class ImageForCommandCategoryAdmin(admin.ModelAdmin):

    def has_add_permission(self, obj):
        return self.model.objects.count() < 1


@admin.register(ImageForBanner)
class ImageForBannerAdmin(admin.ModelAdmin):
    list_display = ('page_name', )


class ColumnTableSizeNested(nested_admin.NestedTabularInline):
    model = ColumnTableSize
    extra = 0


class TableRowNested(nested_admin.NestedTabularInline):
    model = RowTableSize
    inlines = (ColumnTableSizeNested, )
    extra = 0


@admin.register(TableSize)
class TableSizeAdmin(nested_admin.NestedModelAdmin):
    inlines = (TableRowNested, )
    extra = 0

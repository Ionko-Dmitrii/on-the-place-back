from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import (
    LoginView, PasswordResetView, PasswordResetDoneView,
    PasswordResetConfirmView, PasswordChangeView, PasswordChangeDoneView,
    LogoutView
)
from django.db.models import Prefetch
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import (
    TemplateView, CreateView, DeleteView
)

from apps.account.form import (
    RegistrationForm, UserForm, UserProfileForm, ChangePasswordForm
)
from apps.account.models import User, FavoriteSport, UserProfile, Favorite
from apps.common.models import Subcategory, Category, MainCategory
from core.constants import KIND_OF_SPORTS, MAN, WOMAN, CHILDREN


class UserLoginView(LoginView):
    """Endpoint для входа"""

    template_name = 'pages/login.html'
    redirect_authenticated_user = True
    form_class = AuthenticationForm


class SendLinkForNewPasswordView(TemplateView):
    """Страница для отправки смс на емаил для смены пароля"""

    template_name = 'pages/reset-password.html'


class UserPasswordResetView(PasswordResetView):
    """Endpoint для отправки ссылки на емайл для смены пароля"""

    success_url = reverse_lazy('password_reset_done')


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    """Endpoint сброса пароля"""
    template_name = 'pages/new-password.html'
    success_url = reverse_lazy('login')


class UserPasswordResetDoneView(PasswordResetDoneView):
    """Endpoint сброса пароля завершен"""
    template_name = 'pages/profile.html'


class RegistrationView(CreateView):
    """Endpoint для регистрации пользователя"""

    template_name = 'pages/registration.html'
    form_class = RegistrationForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        category = [MAN, WOMAN, CHILDREN]
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['favorite_categories'] = (
            MainCategory.objects.filter(type_main_category__in=category)
            .prefetch_related(
                Prefetch('categories', queryset=Category.objects
                         .filter(type_category=KIND_OF_SPORTS)))
            .prefetch_related('categories__subcategories')
        )

        return context

    def form_valid(self, form):
        form = self.form_class(self.request.POST)
        user = form.save(commit=False)
        User(
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name'],
            email=form.cleaned_data['email'],
            birthday=form.cleaned_data['birthday'],
            gender=form.cleaned_data['gender'],
            city=form.cleaned_data['city'],
            is_subscribe=form.cleaned_data['is_subscribe'],
        )
        user.set_password(form.cleaned_data['password'])
        user.save()
        favorite_sport_arr = form.cleaned_data['favorite_sport'].split(',')
        favorite_sport = (
            Subcategory.objects.filter(slug__in=favorite_sport_arr)
        )
        email, favorite_sport_created = FavoriteSport.objects.get_or_create(
            user=user)
        for i in favorite_sport:
            email.subcategory.add(i)
        Favorite.objects.create(user=user)
        UserProfile.objects.create(user=user)
        login(self.request, user)

        return JsonResponse(
            dict(succes=True, message='Success', url=self.success_url),
            status=200
        )

    def form_invalid(self, form):
        messages = []
        for i in form.errors:
            messages.append([i, form.errors[i]])

        return JsonResponse(
            dict(succes=False, message=messages), status=400
        )


class TestView(TemplateView):
    template_name = 'pages/new-password-profile.html'


class ChangePasswordView(PasswordChangeView):
    form_class = ChangePasswordForm
    template_name = 'pages/new-password-profile.html'
    success_url = reverse_lazy('profile')


class ChangePasswordDoneView(PasswordChangeDoneView):
    template_name = 'pages/profile.html'
    success_url = reverse_lazy('profile')


class UserLogoutView(LogoutView):
    template_name = 'pages/login.html'


class ProfileView(TemplateView):
    form_class = UserProfileForm

    def get(self, request, *args, **kwargs):
        user_form = UserForm(instance=request.user)
        profile_form = UserProfileForm(instance=request.user.profile)
        image_profile = UserProfile.objects.get(user=request.user).image

        return render(request, 'pages/profile.html', context={
            'user_form': user_form,
            'profile_form': profile_form,
            'image_profile': image_profile,
        })

    def post(self, request, *args, **kwargs):
        user_form = UserForm(instance=request.user, data=request.POST)
        profile_form = UserProfileForm(
            instance=request.user.profile,
            data=request.POST,
            files=request.FILES
        )
        if profile_form.is_valid() and user_form.is_valid():
            user_form.save()
            profile_form.save()

            return redirect('profile')
        else:
            messages = []
            for i in user_form.errors:
                messages.append([i, user_form.errors[i]])
            return JsonResponse(
                dict(success=False, message=messages), status=400)


class DeleteAvatarView(DeleteView):
    model = UserProfile
    queryset = UserProfile.objects.all()

    def get_context_data(self, **kwargs):
        context = super(DeleteAvatarView, self).get_context_data(**kwargs)
        context['profile'] = UserProfile.objects.all()
        context['user'] = User.objects.first()
        return context

    def post(self, request, *args, **kwargs):
        user_id = request.user.id
        UserProfile.objects.get(user_id=user_id).image.delete()

        return JsonResponse(dict(
            success=True, message='success'
        ), status=200)

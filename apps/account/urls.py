from django.contrib.auth.views import PasswordResetCompleteView
from django.urls import path

from apps.account.views import (
    UserLoginView, UserPasswordResetView, SendLinkForNewPasswordView,
    UserPasswordResetConfirmView, UserPasswordResetDoneView, RegistrationView,
    ProfileView, DeleteAvatarView, ChangePasswordView, ChangePasswordDoneView,
    UserLogoutView, TestView
)

urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('send-link/', SendLinkForNewPasswordView.as_view(), name='send-link'),
    path('password-reset/', UserPasswordResetView.as_view(),
         name='password-reset'),
    path(
        'reset/<uidb64>/<token>/', UserPasswordResetConfirmView.as_view(),
        name='password_reset_confirm'
    ),
    path(
        'password_reset/done/', UserPasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    path(
        'reset/done/', PasswordResetCompleteView.as_view(),
        name='password_reset_complete'
    ),

    path('new-password-profile/', TestView.as_view(),
         name='new-password-profile'),

    path('profile-new-password/', ChangePasswordView.as_view(),
         name='password_change'),
    path('change-new-password/done/', ChangePasswordDoneView.as_view(),
         name='profile-new-password-done'),

    path('logout/', UserLogoutView.as_view(), name='logout'),

    path('profile/', ProfileView.as_view(), name='profile'),
    path('delete-avatar', DeleteAvatarView.as_view(), name='delete-avatar'),
]

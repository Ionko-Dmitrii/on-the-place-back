from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from apps.account.models import UserProfile, FavoriteSport, Favorite

User = get_user_model()


class BasketItemInline(admin.StackedInline):
    model = UserProfile
    extra = 1


class FavoriteSportInline(admin.StackedInline):
    model = FavoriteSport
    filter_horizontal = ('subcategory',)


class FavoriteInline(admin.StackedInline):
    model = Favorite
    filter_horizontal = ('product',)
    extra = 1


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'birthday',
            'gender', 'city', 'is_subscribe'
        )}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups',
                       'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_superuser')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    filter_horizontal = ('groups',)
    readonly_fields = (
        'email',
    )
    inlines = (BasketItemInline, FavoriteSportInline, FavoriteInline)

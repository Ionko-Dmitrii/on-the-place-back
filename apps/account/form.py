from django import forms
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError

from apps.account.models import User, UserProfile


class RegistrationForm(forms.ModelForm):
    """Форма для регистрации"""

    password_repeat = forms.CharField(max_length=255)
    favorite_sport = forms.CharField(max_length=255, required=True)
    birthday = forms.DateField(input_formats=['%d.%m.%Y'])

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'birthday', 'gender',
                  'city', 'password', 'is_subscribe')

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_repeat = cleaned_data.get("password_repeat")

        if password != password_repeat:
            raise ValidationError(
                "Пароли не совпадают"
            )


class UserForm(forms.ModelForm):
    birthday = forms.DateField(input_formats=['%d.%m.%Y'])

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name', 'birthday', 'gender', 'city'
        )


class UserProfileForm(forms.ModelForm):
    image = forms.ImageField(required=False)
    region = forms.CharField(required=False)
    street = forms.CharField(required=False)
    home = forms.CharField(required=False)
    apartment = forms.CharField(required=False)

    class Meta:
        model = UserProfile
        fields = (
            'image', 'region', 'street', 'home', 'apartment',
        )


class ChangePasswordForm(forms.ModelForm):

    new_password1 = forms.CharField(max_length=256, required=True)
    new_password2 = forms.CharField(max_length=256, required=True)
    old_password = forms.CharField(max_length=256, required=True)

    class Meta:
        model = User
        fields = ('old_password', 'new_password1', 'new_password2')

    error_messages = {
        'password_mismatch': 'Пароли не совпадают!',
        'password_incorrect': "Ваш старый пароль был введен неверно.",
        'password_len': "Пароль должен состоять из 8 символов.",
    }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        if len(password1) < 8:
            raise ValidationError(
                self.error_messages['password_len'],
                code='password_len'
            )
        password_validation.validate_password(password2, self.user)
        return password2

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        password = self.cleaned_data.get('new_password1')
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user

from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.constants import GENDER_TYPE, MALE
from core.utils import generate_filename


class CustomUserManager(BaseUserManager):
    """
    Диспетчер пользовательских моделей, в котором электронная почта является
    уникальным идентификаторо для аутентификации вместо имен пользователей.
    """

    def _create_user(self, email, password, **extra_fields):
        """
        Создание и сохранения юзера с использовнием email и пароля
        """
        if not email:
            raise ValueError('The Email must be set')
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Создание и сохранения суперюзера с использовнием email и пароля"""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """Модель пользователя"""

    username = None
    email = models.EmailField('E-Mail', unique=True)
    birthday = models.DateField(verbose_name="Дата рождения", null=True)
    gender = models.CharField(
        verbose_name="Пол", max_length=10, choices=GENDER_TYPE, default=MALE
    )
    city = models.CharField(verbose_name='Город', max_length=255, null=True)
    is_subscribe = models.BooleanField(
        verbose_name="Подписаться на новости и акции", default=False
    )

    objects = CustomUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.first_name + " " + self.last_name


class UserProfile(models.Model):
    """Модель профиля для пользователя"""

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile',
        verbose_name='Пользователь'
    )
    image = models.ImageField(
        verbose_name='Изображение', upload_to=generate_filename, null=True
    )
    region = models.CharField(verbose_name='Область', null=True, max_length=255)
    street = models.CharField(verbose_name='Улица', max_length=255)
    home = models.CharField(verbose_name='Дом', max_length=20)
    apartment = models.CharField(
        verbose_name='Квартира', null=True, max_length=20
    )

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профиль пользователя'

    def __str__(self):
        return self.user.email


class FavoriteSport(models.Model):
    """Модель любимых видов спорта для пользователя"""

    subcategory = models.ManyToManyField(
        'common.Subcategory', verbose_name='Любимые виды спорта',
        related_name='favorite_sports'
    )
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='sports_for_user',
        verbose_name='Виды спорта для пользователя'
    )

    class Meta:
        verbose_name = 'Любимые виды спорта'
        verbose_name_plural = 'Любимые виды спорта'

    def __str__(self):
        return self.user.email


class Favorite(models.Model):
    """Модель для избранного"""

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name='Пользователь',
        related_name='favorite_user'
    )
    product = models.ManyToManyField(
        'common.Product', verbose_name='Избранные продукты', blank=True,
        related_name='favorite_products'
    )

    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранные'

    def __str__(self):
        return self.user.email
